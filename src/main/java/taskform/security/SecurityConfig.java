package taskform.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private EmployeeDetailsService employeeDetailsService;

    @Autowired
    private EmployeeAuthenticationSuccessHandler employeeAuthenticationSuccessHandler;

    @Autowired
    private EmployeeAuthenticationFailureHandler employeeAuthenticationFailureHandler;
//    @Override
//    public void configure(HttpSecurity httpSecurity) throws Exception {
//       httpSecurity
//               .antMatcher("/admin/**")
//    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
               .antMatchers("/admin/**").hasRole("ADMIN")
               .antMatchers("/user/**").hasRole("EMPLOYEE")
                .antMatchers("/").permitAll()
                .and().csrf().disable()
                .formLogin()
                .loginPage("/login").permitAll()
                .usernameParameter("email")
                .successHandler(employeeAuthenticationSuccessHandler)
                .failureHandler(employeeAuthenticationFailureHandler);

    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(getDatabaseAuthenticationProvider());
    }

    @Bean
    public AuthenticationProvider getDatabaseAuthenticationProvider() {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setUserDetailsService(employeeDetailsService);
        daoAuthenticationProvider.setPasswordEncoder(getPasswordEncode());
        return daoAuthenticationProvider;
    }

    @Bean
    PasswordEncoder getPasswordEncode() {
        return  new BCryptPasswordEncoder();
    }
}
