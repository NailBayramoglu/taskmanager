package taskform.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import taskform.domain.Employee;
import taskform.domain.EmployeeStatus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class EmployeePrincipal implements UserDetails, Serializable {
    public Employee getEmployee() {
        return employee;
    }

    private Employee employee;

    public EmployeePrincipal(Employee employee) {
        this.employee = employee;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {//todo????
        List<GrantedAuthority>authorities=new ArrayList<>();
        System.out.println("authorities"+authorities);
        //authorities-permission+role

        //role based authority ucun ROLE_ vacibdir onsuz islemeyecek
        employee.getRoleList().forEach(role -> {
            SimpleGrantedAuthority authority=new SimpleGrantedAuthority("ROLE_"+role.name());
            authorities.add(authority);
        });
        return authorities;
    }

    @Override
    public String getPassword() {
        return employee.getPassword();
    }

    @Override
    public String getUsername() {
        return employee.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
