package taskform.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import taskform.domain.Employee;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class EmployeeAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        EmployeePrincipal employeePrincipal= (EmployeePrincipal) authentication.getPrincipal();
//        String page=employeePrincipal.getEmployee().getRoleList().get(0).getSuccespage();
//        httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + page);
        Employee employee=employeePrincipal.getEmployee();
        String page=employee.getRoleList().get(0).getSuccespage();
        System.out.println("employeeeeeee=="+employee);
        System.out.println("page=="+page);
        httpServletResponse.sendRedirect(httpServletRequest.getContextPath()+page);
        httpServletRequest.getSession().setAttribute("user",employee);
    }
}
