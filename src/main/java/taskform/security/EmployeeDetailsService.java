package taskform.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import taskform.domain.Employee;
import taskform.service.EmployeeService;


@Service
public class EmployeeDetailsService implements UserDetailsService {
    @Autowired
    private EmployeeService employeeService;
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Employee employee=employeeService.getEmployeeByEmail(s);
        employee.setRoleList(employeeService.getRoleListByUserId(employee.getId()));
        EmployeePrincipal employeePrincipal=new EmployeePrincipal(employee);

        return employeePrincipal;

    }
}
