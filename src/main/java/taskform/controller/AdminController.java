package taskform.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import taskform.domain.DataTableRequest;
import taskform.domain.DataTableResult;
import taskform.domain.Employee;
import taskform.domain.Task;
import taskform.service.EmployeeService;
import taskform.service.TaskService;
import taskform.validation.TaskValidator;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/admin")
@Controller
public class AdminController {
    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private TaskValidator taskValidator;

    @Autowired
    private TaskService taskService;

    @GetMapping("/")
    public String profile() {
        return "profile";

    }

    @GetMapping("/taskboard")
    public ModelAndView taskBoard() {
        Task task = new Task();
        ModelAndView mav = new ModelAndView("taskboard");
        Map<Long, String> employeeMap = new HashMap<>();
        List<Employee> employeeList = employeeService.getEmployeeList();


        employeeList.forEach(emp -> employeeMap.put(emp.getId(), emp.getName() + "  " + emp.getSurname()));
        List<Task> list = taskService.getTask();
        mav.addObject("taskList", list);
        mav.addObject("task", task);
        mav.addObject("employeeList", employeeList);
        mav.addObject("employeeMap", employeeMap);
        return mav;
    }


    @GetMapping("/addTask")
    public ModelAndView task() {
        Task task = new Task();
        ModelAndView mav = new ModelAndView("taskboard");
        mav.addObject("task", task);
        return mav;
    }

    @PostMapping("/addtask")
    public ModelAndView addTask(
            @ModelAttribute("task")
            @Validated Task task,
            BindingResult bindingResult
    ) {
        ModelAndView mav = new ModelAndView("taskboard");

        if (bindingResult.hasErrors()) {
            System.out.println("error var" + bindingResult);

        } else {

            System.out.println("error yoxdu");
            taskService.addTask(task);
        }
        Map<Long, String> employeeMap = new HashMap<>();
        List<Employee> employeeList = employeeService.getEmployeeList();
        employeeList.forEach(emp -> employeeMap.put(emp.getId(), emp.getName() + "  " + emp.getSurname()));
        mav.addObject("employeeMap", employeeMap);


        return mav;
    }


    @GetMapping("/employee")
    public ModelAndView employee() {
        ModelAndView mav = new ModelAndView("employee");
        Employee employee = new Employee();
        List<Employee> employeeList = employeeService.getEmployeeList();

        mav.addObject("employeeList", employeeList);
        mav.addObject("employee", employee);
        return mav;
    }

    @PostMapping("/addemployee")
    public ModelAndView addEmployee(@ModelAttribute("employee")
                                    @Validated Employee employee,
                                    BindingResult result
    ) {
        ModelAndView mav = new ModelAndView("redirect:/admin/employee");
        try {
            if (!result.hasErrors()) {
                employeeService.addEmployee(employee);

            } else {
                mav.setViewName("employee");
            }
        } catch (Exception e) {
            new SQLException("User not added");
        }
        return mav;
    }

    @PostMapping("/editemployee")
    public ModelAndView editEmployee(@ModelAttribute("employee")
                                             Employee employee,
                                     BindingResult result) {
        ModelAndView mav = new ModelAndView();

        if (!result.hasErrors()) {
            employeeService.updateEmployeeById(employee);
            mav.setViewName("redirect:/admin/employee");
        } else {
            mav.setViewName("editemployee");
        }

        return mav;

    }


    @GetMapping("/task")
    public ModelAndView getTask() {
        ModelAndView mav = new ModelAndView("taskboard");
        Task task = new Task();
        List<Task> list = taskService.getTask();
        mav.addObject("taskList", list);
        mav.addObject("task", task);
        return mav;
    }

    @GetMapping("/profile")
    public String profilePage() {

        return "profile";
    }


    @GetMapping("/getTaskListAjax")
    @ResponseBody
    public DataTableResult empListAjax(@RequestParam(name = "draw") int draw,
                                       @RequestParam(name = "start") int start,
                                       @RequestParam(name = "length") int length,
                                       @RequestParam(name = "order[0][column]") int sortColumn,
                                       @RequestParam(name = "order[0][dir]") String sortDirection,
                                       @RequestParam(name = "search[value]") String filter

    ) {
        DataTableRequest dataTableRequest = new DataTableRequest(draw, start, length, sortColumn, sortDirection, filter);
        System.out.println("dataTableRequest=" + dataTableRequest);

        DataTableResult result = taskService.result(dataTableRequest);
        System.out.println("datatabele result" + result);
        return result;
    }


    @PostMapping("search")
    public ModelAndView searching(@RequestParam("search") String search) {

        ModelAndView mav = new ModelAndView("employee");
        Employee employee = new Employee();

        List<Employee> employeeList = employeeService.getEmployeeListWithPaging(search);

        System.out.println("employeeList==" + employeeList);
        mav.addObject("employeeList", employeeList);
        mav.addObject("employee", employee);
        return mav;

    }

    @PostMapping("/delete")
    public String deletetask(@RequestParam(name = "id") long id) {

        taskService.deleteTask(id);
        return "redirect:/admin/taskboard";
    }

    @GetMapping("/getempbyid")
    public ModelAndView getEmployeeById(@RequestParam(name = "id") long id) {

        Employee employee = employeeService.getEmployeeById(id);

        ModelAndView mav = new ModelAndView("editemployee");
        mav.addObject("employee", employee);
        return mav;
    }


    @GetMapping("/deleteEmployee")
    public String deleteEmployee(@RequestParam(name = "id") long id) {
        employeeService.deleteEmployee(id);
        return "redirect:/admin/employee";
    }

    @GetMapping("/viewemployeetask")
    public ModelAndView viewEmployeeTask(@RequestParam(name = "id") long id,
                                         HttpServletRequest request
    ) {
        ModelAndView mav = new ModelAndView("viewemployeetask");
        List<Task> taskList = taskService.getEmployeeTaskById(id);
        mav.addObject("taskList", taskList);
        return mav;
    }


    @GetMapping("/viewemployee")
    public ModelAndView viewEmployee(@RequestParam(name = "id") long id) {
        ModelAndView mav = new ModelAndView("viewemployee");
        Employee employee = employeeService.getEmployeeById(id);
        mav.addObject("employee", employee);
        return mav;
    }

    @GetMapping("/view")
    public ModelAndView viewTask(@RequestParam(name = "id") long id) {
        Task task = taskService.getTaskById(id);

        ModelAndView mav = new ModelAndView("viewtask");
        mav.addObject("viewtask", task);
        return mav;
    }

    @GetMapping("/delete")
    public String deleteTask(@RequestParam(name = "id") long id) {
        taskService.deleteTask(id);

        return "redirect:/admin/taskboard";

    }

    @PostMapping("/edittask")
    public ModelAndView editTask(@ModelAttribute("edittask")
                                         Task task,
                                 BindingResult result
    ) {
        ModelAndView mav = new ModelAndView();

        if (!result.hasErrors()) {
            taskService.updateTaskById(task);
            mav.setViewName("redirect:/admin/taskboard");
        } else {
            mav.setViewName("edittask");
        }

        return mav;

    }

    @GetMapping("/edittask")
    public ModelAndView getTaskById(@RequestParam(name = "id") long id) {

        Task task = taskService.getTaskById(id);

        ModelAndView mav = new ModelAndView("edittask");
        mav.addObject("edittask", task);
        return mav;
    }

    @PostMapping("/profilepicture")
    public void uploadPicture(
            @RequestParam(name = "image") MultipartFile image
    ) {
        Employee employee = new Employee();
//        fileStorageService.saveFile(employee.getId(), image);
        //  employee.setPofileImage(profileImage);
    }


}
