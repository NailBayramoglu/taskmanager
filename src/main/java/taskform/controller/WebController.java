package taskform.controller;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import taskform.domain.*;
import taskform.service.EmployeeService;
//import taskform.service.FileStorageService;
import taskform.service.TaskService;
import taskform.service.EmailValidationService;
import taskform.validation.EmployeeValidator;
import taskform.validation.TaskValidator;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@RequestMapping("/")
@Controller
public class WebController {
    @Autowired
    private EmployeeValidator employeeValidator;

//    @Autowired
//    private FileStorageService fileStorageService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private TaskValidator taskValidator;

    @Autowired
    private TaskService taskService;


    @InitBinder
    protected void InitBinder(WebDataBinder binder) {
        Object target = binder.getTarget();
        if (target != null) {
            if (target.getClass().equals(Employee.class)) {
                binder.setValidator(employeeValidator);
            }
            if (target.getClass().equals(Task.class)) {
                binder.setValidator(taskValidator);
            }
        }

    }

    @GetMapping("/")
    public String indexPage() {
        return "redirect:/login";

    }
    @GetMapping("/404")
    public String notfound(){
        return "404";
    }



    @GetMapping("/register")
    public ModelAndView register() {
        ModelAndView mav = new ModelAndView("register");

        Employee form = new Employee();
        List<Role> roleList = employeeService.getRoleList();
        Map<Integer, String> roleMap = new HashMap<>();
        roleList.forEach(role -> roleMap.put(role.getValue(), role.name()));

        System.out.println("roleList" + roleList);
        mav.addObject("roleMap", roleMap);
        mav.addObject("empReg", form);
        return mav;
    }

    @PostMapping("/register")
    public ModelAndView registerEmployee(@ModelAttribute("empReg")
                                         @Validated Employee form,
                                         BindingResult validationResult
    ) {
        ModelAndView mav = new ModelAndView("/register");

        if (validationResult.hasErrors()) {

            mav.setViewName("register");
        } else {
            employeeService.register(form);
            mav.setViewName("redirect:/login");

        }
        return mav;
    }


    @GetMapping("/login")
    public String loginPage(HttpServletRequest request) {
            request.getSession().invalidate();

        return "login";
    }

//                             INTERCEPTOR VERSION
//    @PostMapping("/login")
//    public ModelAndView checkLogin(@RequestParam(name = "email") String email,
//                                   @RequestParam(name = "password") String password,
//                                   HttpServletRequest request
//    ) {
//        ModelAndView mav = new ModelAndView();
//        Employee employee = employeeService.getEmployeeByEmail(email);
//
//        employee.setRoleList(employeeService.getRoleListByUserId(employee.getId()));
//        System.out.println("rolelist"+employee.getRoleList());
//        String message = "";
//        if (BCrypt.checkpw(password, employee.getPassword())) {
//            String page = employee.getRoleList().get(0).getSuccespage();
//            System.out.println("page"+page);
//            mav.setViewName("redirect:" + page);
//        } else {
//            System.out.println("error login");
//            mav.setViewName("redirect:/login");
//            message = "Incorrect Password";
//            mav.addObject("errorMessage", message);
//        }
//        request.getSession().setAttribute("user", employee);
//        return mav;
//    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request){
        if (request.getSession()!=null){
            request.getSession().invalidate();
        }

        return "redirect:/login";
    }


    //todo sekil elave etmek
    //todo REST

}
