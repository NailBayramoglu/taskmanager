package taskform.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import taskform.domain.DataTableRequest;
import taskform.domain.DataTableResult;
import taskform.domain.Employee;
import taskform.domain.Task;
import taskform.service.EmployeeService;
import taskform.service.TaskService;
import taskform.validation.TaskValidator;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/user")
@Controller
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private TaskValidator taskValidator;

    @Autowired
    private TaskService taskService;

    @GetMapping("/")
    public String empProfile(HttpServletRequest request) {

                    return "employee/employeeprofile";


    }

    @GetMapping("/taskboardemployee")
    public ModelAndView tasBoard() {
        Task task = new Task();  //bos task obyekti yaradiriq
        ModelAndView mav = new ModelAndView("employee/taskboardemployee");//mav da view name ni qeyd edirik
        Map<Long, String> employeeMap = new HashMap<>();
        //id ve employee ucun map
        List<Employee> employeeList = employeeService.getEmployeeList();
        employeeList.forEach(emp -> employeeMap.put(emp.getId(), emp.getName() + "  " + emp.getSurname()));
        //employeeListde gezisirik ve empmapa emp in id name ve surname sin elave edirik
        List<Task> list = taskService.getTask();
        mav.addObject("taskList", list);//tasklar ucun list
        mav.addObject("task", task);//bos task obyekti
        mav.addObject("employeeList", employeeList);//employee lerin listi ucun
        mav.addObject("employeeMap", employeeMap);//employenin id ve ad familyasi taskdaki
        return mav;
    }



    @GetMapping("/employeelist")
    public ModelAndView employee() {
        //bos mav yaratdiq,
        ModelAndView mav = new ModelAndView("employee/employeelist");
        //employee den obyekt yaratdiq
        Employee employee = new Employee();
        //employee leri list seklinde almaq ucun
        List<Employee> employeeList = employeeService.getEmployeeList();
        mav.addObject("employeeList", employeeList);
        //burda jsp deki attribute name i veririk ve icerisine employee ni otururuk
        mav.addObject("employee", employee);

        return mav;
    }


    @GetMapping("/task")
    public ModelAndView getTask() {
        ModelAndView mav = new ModelAndView("employee/taskboardemployee");
        Task task = new Task();
        List<Task> list = taskService.getTask();
        mav.addObject("taskList", list);
        mav.addObject("task", task);
        return mav;
    }

    @GetMapping("/profile")
    public String profilePage() {

        return "profile";
    }


    @GetMapping("/getTaskListAjax")
    @ResponseBody
    public DataTableResult empListAjax(@RequestParam(name = "draw") int draw,
                                       @RequestParam(name = "start") int start,
                                       @RequestParam(name = "length") int length,
                                       @RequestParam(name = "order[0][column]") int sortColumn,
                                       @RequestParam(name = "order[0][dir]") String sortDirection,
                                       @RequestParam(name = "search[value]") String filter

    ) {
        DataTableRequest dataTableRequest = new DataTableRequest(draw, start, length, sortColumn, sortDirection, filter);
        System.out.println("dataTableRequest=" + dataTableRequest);

        DataTableResult result = taskService.result(dataTableRequest);
        System.out.println("datatabele result" + result);
        return result;
    }


    @PostMapping("search")
    public ModelAndView searching(@RequestParam("search") String search) {

        ModelAndView mav = new ModelAndView("employeelist");
        Employee employee = new Employee();

        List<Employee> employeeList = employeeService.getEmployeeListWithPaging(search);

        System.out.println("employeeList==" + employeeList);
        mav.addObject("employeeList", employeeList);
        mav.addObject("employee", employee);
        return mav;

    }


    @GetMapping("/getempbyid")
    public ModelAndView getEmployeeById(@RequestParam(name = "id") long id) {

        Employee employee = employeeService.getEmployeeById(id);

        ModelAndView mav = new ModelAndView("editemployee");
        mav.addObject("employee", employee);
        return mav;
    }




    @GetMapping("/viewemployee")
    public ModelAndView viewEmployee(@RequestParam(name = "id") long id) {
        ModelAndView mav = new ModelAndView();
        Employee employee = employeeService.getEmployeeById(id);
        mav.addObject("employee", employee);
        return mav;
    }

    @GetMapping("/view")
    public String viewTask() {

        return "edittask";
    }


    @PostMapping("/view")
    public ModelAndView viewTask(@RequestParam(name = "id") long id) {
        ModelAndView mav = new ModelAndView("edittask");
        Task task = taskService.getTaskById(id);
        mav.addObject("task", task);
        return mav;
    }

    @PostMapping("/profilepicture")
    public void uploadPicture(
            @RequestParam(name = "image") MultipartFile image
    ) {
        Employee employee = new Employee();
//        fileStorageService.saveFile(employee.getId(), image);
        //  employee.setPofileImage(profileImage);
    }


}
