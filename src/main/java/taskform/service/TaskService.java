package taskform.service;

import taskform.domain.*;

import java.util.List;

public interface TaskService {
    Task addTask(Task task);
    List<Task> getTask();
    List<Employee> getEmployeeList(long taskId);
    DataTableResult result(DataTableRequest request);
    long deleteTask(long id);
    Task getTaskById(long id);

    List<Task> getEmployeeTaskById(long id);

    void updateTaskById(Task task);




}
