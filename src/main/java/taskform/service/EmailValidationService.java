package taskform.service;

public interface EmailValidationService {
    boolean isDuplicate(String email);
}
