package taskform.service.impl;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Service;
import taskform.service.PasswordService;
@Service
public class PasswordServiceImpl implements PasswordService {
    @Override
    public String hashPassword(String clearPassword) {
        return BCrypt.hashpw(clearPassword,BCrypt.gensalt());
    }
}
