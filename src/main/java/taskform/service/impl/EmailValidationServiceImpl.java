package taskform.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import taskform.repository.EmailValidationRepository;
import taskform.service.EmailValidationService;
@Service
public class EmailValidationServiceImpl implements EmailValidationService {
    @Autowired
    private EmailValidationRepository emailValidationRepository;
    @Override
    public boolean isDuplicate(String email) {
        return emailValidationRepository.isDublicate(email);
    }
}
