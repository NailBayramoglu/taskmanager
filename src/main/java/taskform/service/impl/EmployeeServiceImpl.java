package taskform.service.impl;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import taskform.domain.DataTableRequest;
import taskform.domain.DataTableResult;
import taskform.domain.Employee;
import taskform.domain.Role;
import taskform.repository.EmployeeRepository;
import taskform.service.EmployeeService;
import taskform.service.PasswordService;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private PasswordService passwordService;

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public Employee addEmployee(Employee employee) {
        return employeeRepository.addEmployee(employee);
    }

    @Override
    public void saveProfileImage(String image) {

    }

    @Override
    public Employee getEmployeeByEmail() {
        return null;
    }

    @Override
    public List<Role> getRoleListByUserId(long id) {
        return employeeRepository.getRoleListByUserId(id);
    }

    @Override
    public List<Role> getRoleList() {
        return employeeRepository.getRoleList();
    }

    @Override
    public Employee getEmployeeByEmail(String email) {

        return employeeRepository.getEmployeeByEmail(email);
    }

    @Override
    public void addRole(Employee employee, Role role) {
        employeeRepository.addRole(employee, role);
    }

    @Override
    public Employee register(Employee employee) {
        String hashPassword = passwordService.hashPassword(employee.getPassword());
        employee.setPassword(hashPassword);
        return employeeRepository.addEmployee(employee);
    }

    @Override
    public List<Employee> getEmployeeList() {
        return employeeRepository.getEmployeeList();
    }


    @Override
    public List<Employee> getEmployeeListWithPaging(String search) {
        return employeeRepository.getEmployeeListWithPaging(search);
    }


    @Override
    public void updateEmployeeById(Employee employee) {
        employeeRepository.updateEmployeeById(employee);
    }

    @Override
    public long deleteEmployee(long id) {
        return employeeRepository.deleteEmployee(id);
    }


    @Override
    public Employee getEmployeeById(long id) {
        return employeeRepository.getEmployeeById(id);
    }
}
