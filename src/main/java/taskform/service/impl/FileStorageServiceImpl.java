//package taskform.service.impl;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.core.io.FileUrlResource;
//import org.springframework.core.io.Resource;
//import org.springframework.stereotype.Service;
//import org.springframework.web.multipart.MultipartFile;
//import taskform.domain.Employee;
//import taskform.service.FileStorageService;
//import taskform.util.FileUtility;
//
//import java.io.File;
//import java.io.IOException;
//import java.net.MalformedURLException;
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.nio.file.Paths;
//import java.time.LocalDateTime;
//import java.time.format.DateTimeFormatter;
//
//@Service
//public class FileStorageServiceImpl implements FileStorageService {
//    @Value("${upload.folder}")
//    private String uploadFolder;
//
//    @Override
//    public String saveFile(long id, MultipartFile file) {
//        String filepart2="";
//        try {
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
//        String filepart1=uploadFolder+File.separator;
//         filepart2 = String.format("%s%s%s%s",
//                id,
//                File.separator,
//                LocalDateTime.now().format(formatter),
//                FileUtility.getFileExtension(file.getOriginalFilename())
//        );
//        String fileLocation=filepart1+filepart2;
//
//        System.out.println("file location ==" + fileLocation);
//        Path filepath = Paths.get(fileLocation);
//
//            if (Files.notExists(filepath.getParent())) {
//
//                Files.createDirectory(filepath.getParent());
//                System.out.println("does not exist create dir" + fileLocation);
//            }
//            Files.copy(file.getInputStream(), filepath);
//        }
//
//        catch(IOException e){
//            e.printStackTrace();
//        }
//        return filepart2;
//    }
//
//
//
//
//    @Override
//    public Resource getFile(String filename) {
//        String fileLocation=uploadFolder+File.separator+filename;
//        try {
//            return new FileUrlResource(fileLocation);
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new RuntimeException("Error getting file "+filename);
//        }
//    }
//}
