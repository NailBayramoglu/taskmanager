package taskform.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import taskform.domain.*;
import taskform.repository.SqlQuery;
import taskform.repository.TaskRepository;
import taskform.service.EmployeeService;
import taskform.service.TaskService;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TaskServiceImpl implements TaskService {
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private TaskService taskService;

    @Autowired
    private EmployeeService employeeService;

    @Override
    public long deleteTask(long id) {
        return taskRepository.deleteTask(id);
    }

    @Override
    public Task addTask(Task task) {
        return taskRepository.addTask(task);
    }

    @Override
    public List<Task> getTask() {

        List<Task> taskList = taskRepository.getTask();
        for (Task t : taskList) {
            t.setEmployeeList(getEmployeeList(t.getId()));
        }

        return taskList;

    }


    @Override
    public List<Task> getEmployeeTaskById(long id) {
        return taskRepository.getEmployeeTaskById(id);
    }

    @Override
    public DataTableResult result(DataTableRequest request) {
        Map<Integer, String> dataTableMap = new HashMap<>();
        dataTableMap.put(0, "id");
        dataTableMap.put(1, "title");
        dataTableMap.put(2, "description");
        dataTableMap.put(3, "start_date");
        dataTableMap.put(4, "end_date");
        dataTableMap.put(5, "name");
        String sql = SqlQuery.GET_TASK_LIST_WITH_PAGING
                .replace("{SORT_COLUMN}", dataTableMap.get(request.getSortColumn()))
                .replace("{SORT_DIRECTION}", request.getSortDirection());
        DataTableResult result = new DataTableResult();
        result.setDraw(request.getDraw());

        result.setRecordsTotal(taskRepository.getTaskCount());

        result.setRecordsFiltered(taskRepository.getFilteredtaskCount(request.getFilter()));

        List<Task> taskList = taskRepository.getTaskListWithPaging(sql, request.getStart(), request.getLength(), request.getFilter());

        result.setData(new Object[taskList.size()][7]);

        for (int i = 0; i < taskList.size(); i++) {
            result.getData()[i][0] = taskList.get(i).getId();
            result.getData()[i][1] = taskList.get(i).getTitle();
            result.getData()[i][2] = taskList.get(i).getDescription();
            result.getData()[i][3] = taskList.get(i).getStartDate();
            result.getData()[i][4] = taskList.get(i).getEndDate();
            List<Employee> employeeList = taskRepository.getEmployeeList(taskList.get(i).getId());

            String username = "";
            for (int j = 0; j < employeeList.size(); j++) {
                username = username.concat(String.join(",", employeeList.get(j).getName() + " " + employeeList.get(j).getSurname() + "</br>"));
            }
            result.getData()[i][5] = username;

            result.getData()[i][6] =


                            "<button><a href=\"/admin/view?id="+taskList.get(i).getId()+"\">View</a></button>"
                           + "<button><a href=\"/admin/edittask?id="+taskList.get(i).getId()+"\">Edit</a></button>"
                            + "<button><a href=\"/admin/delete?id="+taskList.get(i).getId()+"\">Delete</a></button>";

        }

                              return result;
    }

    @Override
    public void updateTaskById(Task task) {
         taskRepository.updateTaskById(task);
    }

    @Override
    public List<Employee> getEmployeeList(long taskId) {
        return taskRepository.getEmployeeList(taskId);
    }

    @Override
    public Task getTaskById(long id) {
        Task task = taskRepository.getTaskById(id);
//        task.setEmployee(employeeService.getEmployeeById(id));
        return task;
    }
}
