package taskform.service;

public interface PasswordService {
    String hashPassword(String clearPassword);
}
