package taskform.service;

import taskform.domain.DataTableRequest;
import taskform.domain.DataTableResult;
import taskform.domain.Employee;
import taskform.domain.Role;

import java.util.List;

public interface EmployeeService {
    Employee addEmployee(Employee employee);

    List<Employee>getEmployeeList();
    Employee register(Employee employee);

    List<Employee>getEmployeeListWithPaging(String search);

    long deleteEmployee(long id);
    Employee getEmployeeById(long id);

    void updateEmployeeById(Employee employee);

    void addRole(Employee employee,Role role);

    Employee getEmployeeByEmail(String email);
  List<Role>  getRoleList();
  List<Role>  getRoleListByUserId(long id);

  void saveProfileImage(String image);

    Employee getEmployeeByEmail();
}
