package taskform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskformApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaskformApplication.class, args);
	}

}
