package taskform.domain;

public enum Gender {
    MALE(0),
    FEMALE(1);

    private int value;

    Gender(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
    public Gender gender(int value){
        Gender gender=null;
        if (value==0){
            gender=MALE;
        }
        else if (value==1){
            gender=FEMALE;
        }
        return gender;
    }
}
