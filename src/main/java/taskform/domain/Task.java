package taskform.domain;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class Task implements Serializable {
    private long id;
    private String title;
    private String description;
    private List<Employee> employeeList;
    private Employee employee;
    private String startDate;
    private String endDate;
    private long  [] empId;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public long[] getEmpId() {
        return empId;
    }

    public void setEmpId(long[] empId) {
        this.empId = empId;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", employeeList=" + employeeList +
                ", employee=" + employee +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", empId=" + Arrays.toString(empId) +
                '}';
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
