package taskform.domain;

public enum EmployeeStatus {
    
    ACTIVE(1),
    DEACTIVE(0);

    private int value;

    public int getValue() {
        return value;
    }

    EmployeeStatus(int value) {
        this.value=value;
    }
    public static EmployeeStatus fromStatus(int value){
        EmployeeStatus userStatus=null;
        if (value==0){
            userStatus=DEACTIVE;
        }else if (value==1){
            userStatus=ACTIVE;
        }
        return userStatus;
    }
}
