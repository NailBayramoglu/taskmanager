package taskform.domain;

public enum Role {
    ADMIN(1),
    EMPLOYEE(2);

    private int value;

    private String succespage;
    private int priority;

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }



    public void setValue(int value) {
        this.value = value;
    }

    public String getSuccespage() {
        return succespage;
    }

    public void setSuccespage(String succespage) {
        this.succespage = succespage;
    }

    public static Role fromValue(int value){
        Role userType=null;

        if (value==1){
            userType=ADMIN;
        }

        if (value==2){
            userType=EMPLOYEE;
        }
        return userType;
    }

    public int getValue() {
        return value;
    }

    Role(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Role{" +
                "value=" + value +
                ", succespage='" + succespage + '\'' +
                "} " + super.toString();
    }
}
