package taskform.domain;

public enum TaskStatus {
    PLANNED(1),
    IN_PROGRESS(2),
    COMPLETED(3),
    IN_COMPLETED(4);

    public static TaskStatus status(int value){
        TaskStatus taskStatus=null;
        if (value==1){
            taskStatus=PLANNED;
        }
        if (value==2){
            taskStatus=IN_PROGRESS;
        }
        if (value==3){
            taskStatus=COMPLETED;
        }
        if (value==4){
            taskStatus=IN_COMPLETED;
        }
        return taskStatus;
    }
    private int value;

    public int getValue() {
        return value;
    }

    TaskStatus(int value) {
        this.value = value;
    }
}
