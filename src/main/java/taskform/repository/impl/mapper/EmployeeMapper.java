package taskform.repository.impl.mapper;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import taskform.domain.Employee;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class EmployeeMapper implements RowMapper<Employee> {
    @Override//database deki row lari java obyekte cevirir.
    public Employee mapRow(ResultSet rs, int i) throws SQLException {
        Employee employee = new Employee();

        employee.setId(rs.getLong("id"));
        employee.setName(rs.getString("name"));
        employee.setSurname(rs.getString("surname"));
        employee.setEmail(rs.getString("email"));
        employee.setMobile(rs.getString("phone"));
        employee.setGender(rs.getString("gender"));
        employee.setDepartment(rs.getString("department"));
        employee.setPosition(rs.getString("position"));
        employee.setBirthDate(rs.getDate("birth_date").toString());
        employee.setPassword(rs.getString("password"));
        return employee;
    }
}
