package taskform.repository.impl.mapper;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import taskform.domain.Employee;
import taskform.domain.Task;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component

public class  TaskRowMapper implements RowMapper<Task> {
    @Override
    public Task mapRow(ResultSet resultSet, int i) throws SQLException {
        Task task=new Task();
        task.setId(resultSet.getLong("id"));
        task.setTitle(resultSet.getString("title"));
        task.setDescription(resultSet.getString("description"));
        task.setStartDate(resultSet.getDate("start_date").toString());
        task.setEndDate(resultSet.getDate("end_date").toString());
        return task;
    }
}
