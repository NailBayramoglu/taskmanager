package taskform.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import taskform.repository.EmailValidationRepository;
import taskform.repository.SqlQuery;
import taskform.service.EmailValidationService;

@Repository
@Component
public class EmailValidationRepositoryImpl implements EmailValidationRepository {
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public boolean isDublicate(String email) {

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("email", email);
        Integer count = jdbcTemplate.queryForObject(SqlQuery.CHECK_EMAIL, parameterSource, Integer.class);
        return count > 0;
    }
}
