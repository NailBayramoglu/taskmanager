package taskform.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import taskform.domain.Employee;
import taskform.domain.Role;
import taskform.domain.Task;
import taskform.repository.EmployeeRepository;
import taskform.repository.SqlQuery;
import taskform.repository.impl.mapper.EmployeeMapper;
import taskform.service.EmployeeService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


@Repository

public class EmployeeRepositoryJdbcImpl implements EmployeeRepository {
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private JdbcTemplate simpleJdbctemplate;

    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private EmployeeMapper mapper;
    RoleMapper roleMapper=new RoleMapper();
    @Override
    public Employee addEmployee(Employee employee) {
        KeyHolder keyHolder=new GeneratedKeyHolder();

        MapSqlParameterSource params=new MapSqlParameterSource();
        params.addValue("name",employee.getName());
        params.addValue("surname",employee.getSurname());
        params.addValue("email",employee.getEmail());
        params.addValue("phone",employee.getMobile());
        params.addValue("gender",employee.getGender());
        params.addValue("department",employee.getDepartment());
        params.addValue("position",employee.getPosition());
        params.addValue("password",employee.getPassword());
        params.addValue("birthDate",employee.getBirthDate());

       int count= jdbcTemplate.update(SqlQuery.ADD_EMPLOYEE,params,keyHolder);

        if (count>0){

            employee.setId(keyHolder.getKey().longValue());
            for (int i = 0; i <employee.getRoleId().length ; i++) {
                employeeService.addRole(employee,Role.fromValue(employee.getRoleId()[i]));

            }

        }else{
            throw new RuntimeException("redirect:/404"+employee);//todo gedir?
        }
        return employee;
    }

    @Override
    public List<Employee> getEmployeeListWithPaging(String search) {
        Object[] args={"%"+search+"%"};
       List<Employee>employeeList=simpleJdbctemplate.query(SqlQuery.GET_EMPLOYEE_LIST_WITH_PAGING, args , mapper);
        return employeeList;
    }


    @Override
    public void addRole(Employee employee,Role role) {

        SqlParameterSource parameterSource=new MapSqlParameterSource()
                .addValue("user_id",employee.getId())
                .addValue("role_id",role.getValue());


        int count=jdbcTemplate.update(SqlQuery.ADD_USER_ROLE,parameterSource);

    }

    @Override
    public void saveProfileImage(String image) {

    }

    @Override
    public List<Role> getRoleListByUserId(long id) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("id",id);
        return  jdbcTemplate.query(SqlQuery.GET_ROLE_LIST_BY_USER_ID,param,roleMapper);

    }

    @Override
    public List<Role> getRoleList() {
        return jdbcTemplate.query(SqlQuery.GET_ROLE_LIST,roleMapper);
    }

    @Override
    public Employee getEmployeeByEmail(String email) {
        MapSqlParameterSource parameterSource=new MapSqlParameterSource();
        parameterSource.addValue("email",email);
       List<Employee>employeeList=jdbcTemplate.query(SqlQuery.GET_EMPLOYEE_BY_EMAIL,parameterSource,mapper);
        return employeeList.get(0);
    }

    @Override
    public void updateEmployeeById(Employee employee) {
        MapSqlParameterSource parameterSource=new MapSqlParameterSource();
        parameterSource.addValue("name",employee.getName());
        parameterSource.addValue("surname",employee.getSurname());
        parameterSource.addValue("department",employee.getDepartment());
        parameterSource.addValue("position",employee.getPosition());
        parameterSource.addValue("email",employee.getEmail());
        parameterSource.addValue("phone",employee.getMobile());
        parameterSource.addValue("id",employee.getId());
        int count=jdbcTemplate.update(SqlQuery.UPDATE_EMPLOYEE,parameterSource);

    }

    @Override
    public long deleteEmployee(long id) {
        MapSqlParameterSource parameterSource=new MapSqlParameterSource();
        parameterSource.addValue("id",id);

        int count=jdbcTemplate.update(SqlQuery.DELETE_EMPLOYEE,parameterSource);

        return count;
    }

    @Override
    public List<Employee> getEmployeeList() {
        return jdbcTemplate.query(SqlQuery.GET_EMPLOYEE_LIST,mapper);
    }

    @Override
    public Employee getEmployeeById(long id) {
        MapSqlParameterSource parameterSource=new MapSqlParameterSource();

        parameterSource.addValue("id",id);
        List<Employee> getEmployeeById=jdbcTemplate.query(SqlQuery.GET_EMPLOYEE_BY_ID,parameterSource,mapper);
        return getEmployeeById.get(0);
    }
    private  class RoleMapper implements RowMapper<Role> {
        @Override
        public Role mapRow(ResultSet resultSet, int i) throws SQLException {

            Role role=Role.fromValue(resultSet.getInt("id"));
            role.setSuccespage(resultSet.getString("succespage"));
//            role.setPriority(resultSet.getInt("priority"));
            return role;
        }
    }
}
