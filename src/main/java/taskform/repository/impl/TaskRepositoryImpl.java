package taskform.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSourceExtensionsKt;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import taskform.domain.Employee;
import taskform.domain.Task;
import taskform.repository.SqlQuery;
import taskform.repository.TaskRepository;
import taskform.repository.impl.mapper.EmployeeMapper;
import taskform.repository.impl.mapper.TaskRowMapper;
import taskform.service.TaskService;

import java.util.List;

@Repository
public class TaskRepositoryImpl implements TaskRepository {
    @Autowired
    private TaskService taskService;
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private JdbcTemplate simpleJdbcTemplate;

    @Autowired
    private TaskRowMapper taskRowMapper;

    @Autowired
    private EmployeeMapper employeeMapper;

    @Override
    public Task viewTask(Task task) {
        return null;//todo
    }

    @Override
    public long getTaskCount() {
        long count = 0;
        count = simpleJdbcTemplate.queryForObject(SqlQuery.TASK_COUNT, Long.class);
        return count;
    }

    @Override
    public long getFilteredtaskCount(String filter) {
        long count = 0;
        Object[] args = {"%" + filter + "%"};
        count = simpleJdbcTemplate.queryForObject(SqlQuery.GET_FILTER_COUNT, args, Long.class);
        return count;
    }

    @Override
    public void updateTaskById(Task task) {
        System.out.println("task repoda task geldi+++"+task);
        MapSqlParameterSource parameterSource=new MapSqlParameterSource();
        parameterSource.addValue("id",task.getId());
        parameterSource.addValue("title",task.getTitle());
        parameterSource.addValue("description",task.getDescription());
        parameterSource.addValue("startDate",task.getStartDate());
        parameterSource.addValue("endDate",task.getEndDate());

        int count=jdbcTemplate.update(SqlQuery.UPDATE_TASK_BY_ID,parameterSource);
    }

    @Override
    public long deleteTask(long id) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", id);

        int count = jdbcTemplate.update(SqlQuery.DELETE_TASK, parameterSource);
        return count;
    }

    @Override
    public Task getTaskById(long id) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", id);
        List<Task> getTaskList = jdbcTemplate.query(SqlQuery.GET_TASK_BY_ID, parameterSource, taskRowMapper);
        return getTaskList.get(0);
    }

    @Override
    public List<Task> getEmployeeTaskById(long id) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", id);
        List<Task> task = jdbcTemplate.query(SqlQuery.GET_EMPLOYEE_TASK_BY_ID, parameterSource, taskRowMapper);

        return task;
    }

    @Override
    public List<Task> getTask() {
        //query list qaytarir bosda olsa problem olmur.

        List<Task> taskList = jdbcTemplate.query(SqlQuery.GET_ALL_TASK, taskRowMapper);

        // queryForObject en azindan bir obyekt gozleyir gelmeyen zaman error verib cixir.
        return taskList;
    }

    @Override
    public List<Employee> getEmployeeList(long taskId) {
        MapSqlParameterSource param = new MapSqlParameterSource();
        param.addValue("task_id", taskId);
        return jdbcTemplate.query(SqlQuery.GET_EMPLOYEE_LIST_BY_TASK_ID, param, employeeMapper);
    }

    @Override
    public List<Task> getTaskListWithPaging(String sql, int start, int length, String filter) {
        Object[] args = {"%" + filter + "%", start, length};
        List<Task> taskList = simpleJdbcTemplate.query(sql, args, taskRowMapper);
        return taskList;
    }

    @Override
    public Task addTask(Task task) {

        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("title", task.getTitle());
        parameterSource.addValue("description", task.getDescription());
        parameterSource.addValue("start_date", task.getStartDate());
        parameterSource.addValue("end_date", task.getEndDate());

        int count = jdbcTemplate.update(SqlQuery.ADD_TASK, parameterSource, keyHolder);
        if (count > 0) {
            task.setId(keyHolder.getKey().longValue());//elde etdiyimiz keyolderi taskin id ne veririk

            MapSqlParameterSource param = new MapSqlParameterSource();
            param.addValue("task_id", task.getId());

            for (int i = 0; i < task.getEmpId().length; i++) {
                param.addValue("user_id", task.getEmpId()[i]);
                jdbcTemplate.update(SqlQuery.Add_USER_TASK, param);

            }
        }
        return task;
    }
}
