package taskform.repository;

public class SqlQuery {
    public static final String ADD_EMPLOYEE="insert into user (id, name, surname, gender, department, position," +
            " phone, email, password, birth_date) " +
            "values (null, :name, :surname, :gender, :department, :position, :phone," +
            " :email, :password, :birthDate ) ";

    public static final String GET_EMPLOYEE_LIST="select  id,password, name, surname, email, phone, position, department, birth_date, " +
            " gender from user where status=1";


    public static final String ADD_TASK="insert into task (id, title, description, start_date, end_date) " +
            "values(null, :title, :description, STR_TO_DATE(:start_date,'%m/%d/%Y'),STR_TO_DATE(:end_date,'%m/%d/%Y')) ";

    public  static final String Add_USER_TASK=" insert into user_task(user_id,task_id)\n" +
            "values( :user_id, :task_id) ";


    public static  final  String GET_TASK_BY_ID="select t.id, t.title, t.description, t.start_date, t.end_date ,u.name from \n" +
            "\ttask t join user_task ut on ut.task_id=t.id join user u on ut.user_id =u.id where t.id=:id";

    public static final String GET_EMPLOYEE_LIST_BY_TASK_ID=" select u.id, u.name,u.email,u.password,u.phone" +
            ",u.gender,u.department,u.position,u.surname" +
            ",u.birth_date from user u join user_task ut on u.id=ut.user_id join \n" +
            " task t on t.id=ut.task_id\n" +
            "  where t.id= :task_id ";
    public static  final  String GET_ALL_TASK="select id, title, description, start_date, end_date from task  ";

    public static final String GET_EMPLOYEE_LIST_WITH_PAGING=" select name,position" +
            ",gender,null as id, null as password," +
            " null as phone,department,email, birth_date " +
            ", surname from user where concat(name,surname) like ? and status=1 ";

    public static final String GET_TASK_LIST_WITH_PAGING="select id, title, description, " +
            "  start_date, end_date " +
            "from task " +
            "where concat(id, title, description, " +
            "    date_format(start_date, '%d.%m.%Y'), " +
            "    date_format(end_date, '%d.%m.%Y')) like ? " +
            " and status=1 " +
            " order by {SORT_COLUMN} {SORT_DIRECTION} " +
            " limit ?, ?";

    public static final String DELETE_TASK=" update task set status=0 where id=:id ";
                                    
    public static final String UPDATE_TASK_BY_ID= " update task set title= :title,description= :description, " +
            "start_date= :startDate,end_date= :endDate where id= :id";

    public static final String ADD_USER_ROLE = "insert into user_role(id, user_id, role_id) " +
            "values(null, :user_id, :role_id)";

    public  static final String TASK_COUNT="select count(id) as task_count from task where status=1";

    public static final String GET_FILTER_COUNT="select count(*) as task_count from task " +
            "where concat(id,title,description,date_format(start_date,'%d.%m.%Y'),date_format(end_date,'%d.%m.%Y')) " +
            "like ? and status=1";

    public static final String CHECK_EMAIL = "select count(id) cnt " +
            "from user " +
            "where email = :email and status = 1";

    public static final String GET_EMPLOYEE_BY_EMAIL="select id,name,surname,email,phone,password,gender,department" +
            ",position,birth_date from user where email=:email and status=1 ";

    public static final String DELETE_EMPLOYEE=" update user set status=0 where id= :id ";

    public static final String GET_EMPLOYEE_BY_ID="select id, name, birth_date" +
            ",null as gender,null as password, surname, department" +
            ", position, phone, email from user  where status=1 and id=:id";

    public static final String UPDATE_EMPLOYEE="update user set name=:name,surname=:surname" +
            ",department=:department,position=:position,phone=:phone,email=:email  where id=:id ";


    public static final String GET_EMPLOYEE_TASK_BY_ID="select t.id, t.title,t.description" +
            ",u.name ,u.surname ,start_date,end_date from user u join user_task ut " +
            " on u.id=ut.user_id join task t on t.id=ut.task_id where u.id=:id ";

    public static final String GET_ROLE_LIST=" select id,name,succespage,priority from role " +
            "where status=1 " +
            "order by priority ";


    public static final String GET_ROLE_LIST_BY_USER_ID="select r.id,r.name,r.succespage \n" +
            "from role r \n" +
            "join user_role ur on r.id= ur.role_id\n" +
            "where ur.user_id=:id and status=1\n" +
            "order by priority  ";
                    //TODO
public static final String SAVE_USER_PROFILE_IMAGE=" update user set picture";

}
