package taskform.repository;

import taskform.domain.Employee;
import taskform.domain.Role;
import taskform.domain.Task;

import java.util.List;

public interface EmployeeRepository {

    Employee addEmployee(Employee employee);
    List<Employee> getEmployeeList();
    List<Employee>getEmployeeListWithPaging(String search);
    long deleteEmployee(long id);

    Employee getEmployeeById(long id);

    void updateEmployeeById(Employee employee);

    void addRole(Employee employee,Role role);

    Employee getEmployeeByEmail(String email);


    List<Role>  getRoleList();

    List<Role> getRoleListByUserId(long id);

     void saveProfileImage(String image);
}
