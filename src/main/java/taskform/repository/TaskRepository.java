package taskform.repository;

import taskform.domain.Employee;
import taskform.domain.Task;

import java.util.List;

public interface TaskRepository {
    Task addTask(Task task);
    Task viewTask(Task task);

    List<Task> getTask();
    List<Employee> getEmployeeList(long taskId);

    List<Task>getTaskListWithPaging(String sql, int start, int length, String filter);

    long deleteTask(long id);

    long getTaskCount();

    void updateTaskById(Task task);

    long getFilteredtaskCount(String filter);

    List<Task> getEmployeeTaskById(long id);

    Task getTaskById(long id);


}
