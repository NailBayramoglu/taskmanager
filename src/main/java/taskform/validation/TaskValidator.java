package taskform.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import taskform.domain.Employee;
import taskform.domain.Task;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
@Component
public class TaskValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(Task.class);
    }

    @Override
    public void validate(Object o, Errors errors) {

        Task task=(Task) o;
        LocalDate startDate=null;
        LocalDate endDate=null;
        DateTimeFormatter formatter=DateTimeFormatter.ofPattern("MM/dd/yyyy");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"title","title.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"description","description.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"empId","choose.employee");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"startDate","start.date.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"endDate","end.date.required");


        if (!errors.hasFieldErrors("startDate")){
            if (task.getTitle()!=null){
                startDate=LocalDate.parse(task.getStartDate(),formatter);
            }
        }

        if (!errors.hasFieldErrors("endDate")){
            if (task.getEndDate()!=null){
                endDate=LocalDate.parse(task.getEndDate(),formatter);
            }
        }
        if (task.getStartDate()==null){
            errors.rejectValue("startDate","start.date.null");
        }
        if (task.getEndDate()==null){
            errors.rejectValue("endDate","end.date.null");
        }



    }


}
