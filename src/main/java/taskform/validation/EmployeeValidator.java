package taskform.validation;

import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import taskform.domain.Employee;
import taskform.service.EmailValidationService;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class EmployeeValidator implements Validator{
    @Autowired
    private EmailValidationService emailValidationService;
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(Employee.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Employee employee = (Employee) o;
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate birth_date = null;


        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "first.name.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "surname", "last.name.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "gender", "gender.selection");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "department", "department.name.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "position", "position.name.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mobile", "mobile.number.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "email.address.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "password.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "passwordConfirm", "password.confirm.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "acceptTerms", "accept.terms.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "birthDate", "birthdate.option");

        String regex = ("(?<=|^)[a-zA-Z\\s]*(?=[.,;:]?|$)");
        if (!errors.hasFieldErrors("name")) {
            if (employee.getName() != null) {
                if (!GenericValidator.matchRegexp(employee.getName(), regex)) {
                    errors.rejectValue("name", "error.name.option");
                }
            }
        }

        if (!errors.hasFieldErrors("surname")) {
            if (employee.getSurname() != null) {
                if (!GenericValidator.matchRegexp(employee.getSurname(), regex)) {
                    errors.rejectValue("surname", "error.surname.option");
                }
            }
        }

        if (!errors.hasFieldErrors("gender")) {
            if (employee.getGender() == null || Integer.parseInt(employee.getGender()) == -1) {
                errors.rejectValue("gender", "error.gender.option");
            }
        }

        if (!errors.hasFieldErrors("department")) {
            if (employee.getDepartment() == null) {
                errors.rejectValue("department", "error.department.option");
            }
        }

        if (!errors.hasFieldErrors("position")) {
            if (employee.getPosition() == null) {
                errors.rejectValue("position", "error.position.option");
            }
        }

        String phoneRegex = ("\\d{10}|(?:\\d{3}-){2}\\d{4}|\\(\\d{3}\\)\\d{3}-?\\d{4}");
        if (!errors.hasFieldErrors("mobile")) {
            if (employee.getMobile() != null) {
                if (!GenericValidator.matchRegexp(employee.getMobile(), phoneRegex))
                    errors.rejectValue("mobile", "error.mobile.option");
            }
        }

        String emailRegex = ("^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$");

        if (!errors.hasFieldErrors("email")) {
            if (employee.getEmail() != null) {
                if (!GenericValidator.matchRegexp(employee.getEmail(), emailRegex)) {
                    errors.rejectValue("email", "error.email.option");
                }
            }
        }
        if (!errors.hasFieldErrors("email")){
            if (employee.getEmail()!=null){
                if (emailValidationService.isDuplicate(employee.getEmail())){
                    errors.rejectValue("email","duplicate.email.option");
                }
            }
        }
        String passwordRegex = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";
        if (!errors.hasFieldErrors("password")) {
            if (employee.getPassword() != null) {
                if (!GenericValidator.matchRegexp(employee.getPassword(), passwordRegex)) {
                    errors.rejectValue("password", "error.password.option");
                }
            }
        }

        if (!errors.hasFieldErrors("passwordConfirm")) {
            if (employee.getPasswordConfirm() != null) {
                if (!employee.getPassword().equals(employee.getPasswordConfirm())) {
                    errors.rejectValue("passwordConfirm", "password.confirm.option");
                }
            }
        }

        if (!errors.hasFieldErrors("birthDate")) {
            if (employee.getBirthDate() != null) {
                birth_date = LocalDate.parse(employee.getBirthDate(), dateTimeFormatter);
                if (birth_date.isAfter(LocalDate.now())){
                    errors.rejectValue("birthDate","birth.date.is.after");
                }
            }
        }


    }
}
