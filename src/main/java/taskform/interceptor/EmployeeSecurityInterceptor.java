package taskform.interceptor;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.AsyncHandlerInterceptor;
import taskform.domain.Employee;
import taskform.domain.Role;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URI;

//@Component
public class EmployeeSecurityInterceptor implements AsyncHandlerInterceptor {

    private static Logger log = Logger.getLogger(EmployeeSecurityInterceptor.class);



    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        boolean success=false;

        if (request.getSession().getAttribute("user")!=null){
            Employee employee= (Employee) request.getSession().getAttribute("user");

            URI uri = new URI(request.getRequestURL().toString());
            String url = getNormalizedUrl(uri, request);
            String requestUrl = request.getRequestURL().toString();

            if (requestUrl.startsWith(url+"admin/") && hasRole(employee,Role.ADMIN)){

                    success=true;

            }
            else if (requestUrl.startsWith(url+"user/")&&hasRole(employee,Role.EMPLOYEE)){
                success=true;
            }
            else {
                response.sendRedirect("/");

            }

        }else {
            response.sendRedirect("/");
            success=false;
        }
        return success;
    }

    private static String getNormalizedUrl(URI uri, HttpServletRequest request) {
        String url = "";
        if(uri.getPort() != -1) {
            url = uri.getScheme() + "://" + uri.getHost() + ":" + uri.getPort() + "/" + request.getContextPath();
        } else {
            url = uri.getScheme() + "://" + uri.getHost() + "/" + request.getContextPath();
        }
        return url;
    }


    public static boolean hasRole(Employee user, Role role) {
        return user.getRoleList().stream().anyMatch(r -> r == role);
    }
}
