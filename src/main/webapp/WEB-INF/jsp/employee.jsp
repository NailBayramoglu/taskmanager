<%--
  Created by IntelliJ IDEA.
  User: nailbayramoglu
  Date: 24.11.19
  Time: 01:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!doctype html>
<html lang="en" dir="ltr">

<!-- Mirrored from puffintheme.com/craft/soccer/project/project-clients.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Nov 2019 08:08:51 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="icon" href="favicon.ico" type="image/x-icon"/>

    <title>Employees</title>

    <!-- Bootstrap Core and vandor -->
    <link rel="stylesheet" href="../assets/plugins/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../assets/plugins/dropify/css/dropify.min.css">

    <!-- Core css -->
    <link rel="stylesheet" href="../assets/css/main.css"/>
    <link rel="stylesheet" href="../assets/css/theme1.css"/>
</head>

<body class="font-montserrat">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
    </div>
</div>

<div id="main_content">

    <jsp:include page="common/header-top.jsp"/>
    <div id="rightsidebar" class="right_sidebar">
        <a href="javascript:void(0)" class="p-3 settingbar float-right"><i class="fa fa-close"></i></a>
        <div class="p-4">
            <div class="mb-4">
                <h6 class="font-14 font-weight-bold text-muted">Font Style</h6>
                <div class="custom-controls-stacked font_setting">
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="font" value="font-opensans">
                        <span class="custom-control-label">Open Sans Font</span>
                    </label>
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="font" value="font-montserrat" checked="">
                        <span class="custom-control-label">Montserrat Google Font</span>
                    </label>
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="font" value="font-roboto">
                        <span class="custom-control-label">Robot Google Font</span>
                    </label>
                </div>
            </div>
            <hr>
            <div class="mb-4">
                <h6 class="font-14 font-weight-bold text-muted">Dropdown Menu Icon</h6>
                <div class="custom-controls-stacked arrow_option">
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="marrow" value="arrow-a">
                        <span class="custom-control-label">A</span>
                    </label>
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="marrow" value="arrow-b">
                        <span class="custom-control-label">B</span>
                    </label>
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="marrow" value="arrow-c" checked="">
                        <span class="custom-control-label">C</span>
                    </label>
                </div>
                <h6 class="font-14 font-weight-bold mt-4 text-muted">SubMenu List Icon</h6>
                <div class="custom-controls-stacked list_option">
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="listicon" value="list-a" checked="">
                        <span class="custom-control-label">A</span>
                    </label>
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="listicon" value="list-b">
                        <span class="custom-control-label">B</span>
                    </label>
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="listicon" value="list-c">
                        <span class="custom-control-label">C</span>
                    </label>
                </div>
            </div>
            <hr>
            <div>
                <h6 class="font-14 font-weight-bold mt-4 text-muted">General Settings</h6>
                <ul class="setting-list list-unstyled mt-1 setting_switch">
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Night Mode</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-darkmode">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Fix Navbar top</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-fixnavbar">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Header Dark</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-pageheader" checked="">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Min Sidebar Dark</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-min_sidebar">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Sidebar Dark</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-sidebar">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Icon Color</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-iconcolor">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Gradient Color</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-gradient">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Box Shadow</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-boxshadow">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">RTL Support</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-rtl">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Box Layout</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-boxlayout">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                </ul>
            </div>
            <hr>
            <div class="form-group">
                <label class="d-block">Storage <span class="float-right">77%</span></label>
                <div class="progress progress-sm">
                    <div class="progress-bar" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 77%;"></div>
                </div>
                <button type="button" class="btn btn-primary btn-block mt-3">Upgrade Storage</button>
            </div>
        </div>
    </div>

    <div class="theme_div">
        <div class="card">
            <div class="card-body">
                <ul class="list-group list-unstyled">
                    <li class="list-group-item mb-2">
                        <p>Default Theme</p>
                        <a href="index-2.html"><img src="../assets/images/themes/default.png" class="img-fluid" /></a>
                    </li>
                    <li class="list-group-item mb-2">
                        <p>Night Mode Theme</p>
                        <a href="http://puffintheme.com/craft/soccer/project-dark/index.html"><img src="../assets/images/themes/dark.png" class="img-fluid" /></a>
                    </li>
                    <li class="list-group-item mb-2">
                        <p>RTL Version</p>
                        <a href="http://puffintheme.com/craft/soccer/project-rtl/index.html"><img src="../assets/images/themes/rtl.png" class="img-fluid" /></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="user_div">
        <jsp:include page="common/brand-name.jsp"/>
        <div class="card-body">
            <a href="page-profile.html"><img class="card-profile-img" src="../assets/images/sm/avatar1.jpg" alt=""></a>
            <h6 class="mb-0">Peter Richards</h6>
            <span>peter.richard@gmail.com</span>
            <div class="d-flex align-items-baseline mt-3">
                <h3 class="mb-0 mr-2">9.8</h3>
                <p class="mb-0"><span class="text-success">1.6% <i class="fa fa-arrow-up"></i></span></p>
            </div>
            <div class="progress progress-xs">
                <div class="progress-bar" role="progressbar" style="width: 15%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar bg-info" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar bg-success" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar bg-orange" role="progressbar" style="width: 5%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar bg-indigo" role="progressbar" style="width: 13%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <h6 class="text-uppercase font-10 mt-1">Performance Score</h6>
            <hr>
            <p>Activity</p>
            <ul class="new_timeline">
                <li>
                    <div class="bullet pink"></div>
                    <div class="time">11:00am</div>
                    <div class="desc">
                        <h3>Attendance</h3>
                        <h4>Computer Class</h4>
                    </div>
                </li>
                <li>
                    <div class="bullet pink"></div>
                    <div class="time">11:30am</div>
                    <div class="desc">
                        <h3>Added an interest</h3>
                        <h4>“Volunteer Activities”</h4>
                    </div>
                </li>
                <li>
                    <div class="bullet green"></div>
                    <div class="time">12:00pm</div>
                    <div class="desc">
                        <h3>Developer Team</h3>
                        <h4>Hangouts</h4>
                        <ul class="list-unstyled team-info margin-0 p-t-5">
                            <li><img src="../assets/images/xs/avatar1.jpg" alt="Avatar"></li>
                            <li><img src="../assets/images/xs/avatar2.jpg" alt="Avatar"></li>
                            <li><img src="../assets/images/xs/avatar3.jpg" alt="Avatar"></li>
                            <li><img src="../assets/images/xs/avatar4.jpg" alt="Avatar"></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <div class="bullet green"></div>
                    <div class="time">2:00pm</div>
                    <div class="desc">
                        <h3>Responded to need</h3>
                        <a href="javascript:void(0)">“In-Kind Opportunity”</a>
                    </div>
                </li>
                <li>
                    <div class="bullet orange"></div>
                    <div class="time">1:30pm</div>
                    <div class="desc">
                        <h3>Lunch Break</h3>
                    </div>
                </li>
                <li>
                    <div class="bullet green"></div>
                    <div class="time">2:38pm</div>
                    <div class="desc">
                        <h3>Finish</h3>
                        <h4>Go to Home</h4>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <jsp:include page="common/sidebar.jsp"/>

    <div class="page">
        <jsp:include page="common/page-top.jsp"/>
        <div class="section-body mt-3">
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-md-flex justify-content-between mb-2">
                                    <ul class="nav nav-tabs b-none">
                                        <li class="btn btn-primary btn-block mb-1" style="color: yellow"><a href="/register"> Add New</a></li>
<%--                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#list"><i class="fa fa-list-ul"></i> Employee List</a></li>--%>
                                    </ul>
                                </div>
                                <div class="row">
                                    <form method="post" action="search">
                                    <div class="nav nav-tabs b-none">
                                        <div class="input-group mb-1">
                                            <input type="text" class="form-control" placeholder="NameSurname" name="search">
                                        </div>
                                    </div>
                                    <%--<div class="nav nav-tabs b-none">
                                        <a href="javascript:void(0);" class="btn btn-primary btn-block mb-1" title="">Search</a>
&lt;%&ndash;                                           <li class="btn btn-primary btn-block mb-1" style="color: yellow"> Search </li>&ndash;%&gt;
                                    </div>--%>
                                        <input class="btn btn-primary btn-block mb-1" type="submit">
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-body">
            <div class="container-fluid">
                <div class="tab-content">
                    <div class="tab-pane fade" id="addnew" role="tabpanel">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Add Employee</h3>
                                        <div class="card-options ">
                                            <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                            <a href="#" class="card-options-remove" data-toggle="card-remove"><i class="fe fe-x"></i></a>
                                        </div>
                                    </div>
                                    <form:form method="post" modelAttribute="employee" action="update?id=${id}">
                                    <div class="row clearfix">

                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label>First Name</label>
                                                <form:input path="name" type="text"/><br/>
                                                <span style="color: red"><form:errors path="name" /> </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label>Last Name</label>
                                                <form:input path="surname" type="text"/><br/>
                                                <span style="color: red"><form:errors path="surname"/> </span>
                                            </div>
                                        </div>

                                            <%--                                            <div class="col-md-3 col-sm-12">--%>
                                            <%--                                                <div class="form-group">--%>
                                            <%--                                                    <label>Date of Birth</label>--%>
                                            <%--                                                    <input data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Date of Birth">--%>
                                            <%--                                                </div>--%>
                                            <%--                                            </div>--%>
                                        <div class="col-md-3 col-sm-12">
                                            <label>Gender</label>
                                            <form:select path="gender">
                                                <option value="-1">-- Gender --</option>
                                                <option value="0">Male</option>
                                                <option value="1">Female</option>
                                            </form:select><br/>
                                            <span style="color: red"><form:errors path="gender" /></span>
                                        </div>
                                        <div class="col-md-3 col-sm-12">
                                            <div class="form-group">
                                                <label>Department</label>
                                                <form:input path="department" type="text"/><br/>
                                                <span style="color: red"><form:errors path="department"/> </span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12">
                                            <div class="form-group">
                                                <label>Position</label>
                                                <form:input path="position" type="text"/><br/>
                                                <span style="color: red"><form:errors path="position"/> </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-group">
                                                <label>Mobile</label>
                                                <form:input type="text" path="mobile"/><br/>
                                                <span style="color: red"><form:errors path="mobile"/> </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <form:input path="email" type="email"/><br/>
                                                <span style="color: red"><form:errors path="email"/> </span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12">
                                            <div class="form-group">
                                                <label>Password</label>
                                                <form:input path="password" type="password"/><br/>
                                                <span style="color: red"><form:errors path="password"/> </span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12">
                                            <div class="form-group">
                                                <label>Password Confirm</label>
                                                <form:input path="passwordConfirm" type="password"/><br/>
                                                <span style="color: red"><form:errors path="passwordConfirm"/> </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <form:checkbox  path="acceptTerms" value="OK"/>
                                                <span>I Agree to the terms and conditions</span>
                                            </label>
                                        </div>&nbsp;
                                            <%--                                            <div class="col-md-4 col-sm-12">--%>
                                            <%--                                                <div class="form-group">--%>
                                            <%--                                                    <label>Website URL</label>--%>
                                            <%--                                                    <input type="text" class="form-control">--%>
                                            <%--                                                </div>--%>
                                            <%--                                            </div>--%>
                                            <%--                                            <div class="col-md-6 col-sm-12">--%>
                                            <%--&lt;%&ndash;                                                <div class="form-group">&ndash;%&gt;--%>
                                            <%--&lt;%&ndash;                                                    <label>Facebook</label>&ndash;%&gt;--%>
                                            <%--&lt;%&ndash;                                                    <input type="text" class="form-control">&ndash;%&gt;--%>
                                            <%--&lt;%&ndash;                                                </div>&ndash;%&gt;--%>
                                            <%--                                            </div>--%>
                                            <%--&lt;%&ndash;                                            <div class="col-md-6 col-sm-12">&ndash;%&gt;--%>
                                            <%--&lt;%&ndash;                                                <div class="form-group">&ndash;%&gt;--%>
                                            <%--&lt;%&ndash;                                                    <label>Twitter</label>&ndash;%&gt;--%>
                                            <%--&lt;%&ndash;                                                    <input type="text" class="form-control">&ndash;%&gt;--%>
                                            <%--&lt;%&ndash;                                                </div>&ndash;%&gt;--%>
                                            <%--                                            </div>--%>
                                            <%--                                            <div class="col-md-6 col-sm-12">--%>
                                            <%--                                                <div class="form-group">--%>
                                            <%--                                                    <label>LinkedIN</label>--%>
                                            <%--                                                    <input type="text" class="form-control">--%>
                                            <%--                                                </div>--%>
                                            <%--                                            </div>--%>
                                            <%--                                            <div class="col-md-6 col-sm-12">--%>
                                            <%--                                                <div class="form-group">--%>
                                            <%--                                                    <label>Behance</label>--%>
                                            <%--                                                    <input type="text" class="form-control">--%>
                                            <%--                                                </div>--%>
                                            <%--                                            </div>--%>
                                            <%--                                            <div class="col-sm-12">--%>
                                            <%--                                                <div class="form-group mt-2 mb-3">--%>
                                            <%--                                                    <input type="file" class="dropify">--%>
                                            <%--                                                    <small id="fileHelp" class="form-text text-muted">This is some placeholder block-level help text for the above input. It's a bit lighter and easily wraps to a new line.</small>--%>
                                            <%--                                                </div>--%>
                                            <%--                                            </div>--%>
                                            <%--                                            <div class="col-sm-12">--%>
                                            <%--                                                <div class="form-group mt-3">--%>
                                            <%--                                                    <label>Messages</label>--%>
                                            <%--                                                    <textarea rows="4" class="form-control no-resize" placeholder="Please type what you want..."></textarea>--%>
                                            <%--                                                </div>--%>
                                            <%--                                            </div>--%>
                                            <%--                                            <div class="col-sm-12">--%>
                                        <button type="submit" class="btn btn-primary">Add</button>
                                        <button type="submit" class="btn btn-outline-secondary">Cancel</button>
                                    </div>
                                </div>
                                </form:form>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade show active" id="list" role="tabpanel">
                        <div class="row clearfix">
                            <style>
                            </style>
                            <c:choose>
                               <c:when test="${ not empty employeeList}">
                                   <c:forEach items="${employeeList}" var="emp" >
                                       <div class="col-xl-4 col-lg-4 col-md-6">
                                           <div class="card">
                                               <div class="card-body text-center ribbon">
                                                   <img class="rounded-circle img-thumbnail w100" src="../assets/images/sm/avatar1.jpg" alt="">
                                                   <h6 class="mt-3 mb-0">${emp.name} ${emp.surname} <br/> ${emp.department} <br/> ${emp.position} <br/>  ${emp.email} <br/> ${emp.mobile} <br/>${emp.birthDate}</h6><br/>

                                                       <%--                                        <ul class="mt-3 list-unstyled d-flex justify-content-center">--%>
                                                       <%--                                            <li><a class="p-3" target="_blank" href="#"><i class="fa fa-facebook"></i></a></li>--%>
                                                       <%--                                            <li><a class="p-3" target="_blank" href="#"><i class="fa fa-slack"></i></a></li>--%>
                                                       <%--                                            <li><a class="p-3" target="_blank" href="#"><i class="fa fa-linkedin"></i></a></li>--%>
                                                       <%--                                        </ul>--%>
                                                    <a style="color: darkred" href="/admin/getempbyid?id=${emp.id}"><h6>Edit</h6></a>
                                                   <a style="color: darkred" href="/admin/viewemployee?id=${emp.id}"><h6>View</h6></a>
                                                   <a style="color: darkred" href="/admin/deleteEmployee?id=${emp.id}"><h6>Delete</h6></a>
                                                        <a style="color: darkred" href="/admin/viewemployeetask?id=${emp.id}"><h6>Tasks</h6></a>
<%--                                                   <div class="row text-center mt-4">--%>
<%--&lt;%&ndash;                                                       <div class="col-6 border-right">&ndash;%&gt;--%>
<%--                                                           --%>
<%--&lt;%&ndash;                                                           <h4 style="align-content: center" class="font-18">07</h4>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                                       </div>&ndash;%&gt;--%>
<%--                                                   </div>--%>
                                               </div>
                                           </div>
                                       </div>
                                   </c:forEach>
                               </c:when>
                            </c:choose>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-body">
            <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            Copyright © 2019 <a href="https://themeforest.net/user/admincraft/portfolio">NailBayramoglu</a>.
                        </div>
<%--                        <div class="col-md-6 col-sm-12 text-md-right">--%>
<%--                            <ul class="list-inline mb-0">--%>
<%--                                <li class="list-inline-item"><a href="http://puffintheme.com/craft/soccer/doc/index.html">Documentation</a></li>--%>
<%--                                <li class="list-inline-item"><a href="javascript:void(0)">FAQ</a></li>--%>
<%--                                <li class="list-inline-item"><a href="javascript:void(0)" class="btn btn-outline-primary btn-sm">Buy Now</a></li>--%>
<%--                            </ul>--%>
<%--                        </div>--%>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>

<script src="../assets/bundles/lib.vendor.bundle.js"></script>

<script src="../assets/plugins/dropify/js/dropify.min.js"></script>

<script src="../assets/js/core.js"></script>
<script src="../assets/js/form/dropify.js"></script>
</body>

<!-- Mirrored from puffintheme.com/craft/soccer/project/project-clients.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Nov 2019 08:08:52 GMT -->
</html>