<%--
  Created by IntelliJ IDEA.
  User: nailbayramoglu
  Date: 24.11.19
  Time: 01:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!doctype html>
<html lang="en" dir="ltr">

<!-- Mirrored from puffintheme.com/craft/soccer/project/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Nov 2019 08:08:56 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="icon" href="favicon.ico" type="image/x-icon"/>

    <title>Task Form Register</title>

    <!-- Bootstrap Core and vandor -->
    <link rel="stylesheet" href="../assets/plugins/bootstrap/css/bootstrap.min.css" />

    <!-- Core css -->
    <link rel="stylesheet" href="../assets/css/main.css"/>

    <link rel="stylesheet" href="../assets/css/theme1.css"/>


</head>
<body class="font-montserrat">
<form:form method="post" modelAttribute="employee" action="/admin/editemployee">
<div class="auth">
    <div class="auth_left">
        <div class="card">
            <div class="text-center mb-5">
                <a class="header-brand" href="index-2.html"><i class="fa fa-handshake-o"></i></a>
            </div>
            <div class="card-body"><br/><br/>
                <div class="card-title"><h4>Edit Profile Information</h4></div>
                <div class="row">

                    <div class="form-group">
                        <label class="form-label">ID</label>
                        <form:input path="id" type="number" readonly="true"/><br/>
                        <span style="color: red"><form:errors path="id" /> </span>
                    </div>&nbsp;

                <div class="form-group">
                    <label class="form-label">Name</label>
                    <form:input path="name" type="text" placeholder="Enter name"/><br/>
                    <span style="color: red"><form:errors path="name" /> </span>
                </div>&nbsp;
                <div class="form-group">
                    <label class="form-label">Surname</label>
                    <form:input type="text" path="surname" placeholder="Enter surname"/><br/>
                    <span style="color: red"><form:errors path="surname"/> </span>
                </div>
                </div>
                <div class="form-group">
                    <label class="form-label">Department</label>
                    <form:input type="text" path="department" placeholder="Enter department"/><br/>
                    <span style="color: red"><form:errors path="department"/> </span>
                </div>
                <div class="form-group">
                    <label class="form-label">Position</label>
                    <form:input type="text" path="position" placeholder="Enter position"/><br/>
                    <span style="color: red"><form:errors path="position"/> </span>
                </div>
                <div class="form-group">
                    <label class="form-label">Phone number</label>
                    <form:input type="text" path="mobile" placeholder="Enter phone number"/><br/>
                    <span style="color: red"><form:errors path="mobile"/></span>
                </div>
                <div class="form-group">
                    <label class="form-label">Email address</label>
                    <form:input type="email" path="email" placeholder="Enter email"/><br/>
                    <span style="color: red"><form:errors path="email"/> </span>
                </div>
                    <label >
                        <form:checkbox  path="acceptTerms" value="OK"/>
                        <span>I Agree to the terms and conditions</span><br/>
                        <span style="color: red"><form:errors path="acceptTerms"/></span>

                    </label>
                </div>
                <div class="form-footer">
                    <button type="submit" class="btn btn-primary btn-block">Confirm Edit</button>
                </div>
            </div>
            <div class="text-center text-muted">
                Already have account? <a href="/login">Sign in</a>
            </div>
        </div>
    </div>
    <div class="auth_right full_img"></div>
</div>
</form:form>
<script src="../assets/bundles/lib.vendor.bundle.js"></script>
<script src="../assets/js/core.js"></script>
</body>

<!-- Mirrored from puffintheme.com/craft/soccer/project/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Nov 2019 08:08:56 GMT -->
</html>