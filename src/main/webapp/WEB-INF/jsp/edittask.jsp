<%--
  Created by IntelliJ IDEA.
  User: nailbayramoglu
  Date: 10.11.19
  Time: 13:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Edit.Task</title>
</head>
<body>


<form:form method="post" modelAttribute="edittask" action="/admin/edittask">

    ID:<form:input path="id"  type="text" /><br/>

<%--    Employees:<form:input path="name" type="text" readonly="true"/><br/>--%>
<%--              <form:input path="surname" type="text" readonly="true"/><br/>--%>

    Title: <form:input path="title" type="text" /><br/>

    Description:<form:input path="description" type="text" /><br/>


    Start date: <form:input path="startDate" type="date" name="start_date" /><br/>


    End date: <form:input path="endDate" type="date" /><br/>

<%--    Name: <form:input path="userName" type="text" placeholder="name" readonly="true"/><br/>--%><br/>

<%--        <a type="button" style="color: darkred" href="/admin/taskboard" ><h2><input type="button" value="Submit Changes"></h2></a>--%>
    <div class="form-footer">
        <button type="submit" class="btn btn-primary btn-block">Confirm And Back To List</button>
    </div>

</form:form>
</body>
</html>
