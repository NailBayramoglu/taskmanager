<%--
  Created by IntelliJ IDEA.
  User: nailbayramoglu
  Date: 10.11.19
  Time: 13:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Edit.Task</title>
</head>
<body>


<form:form method="post" modelAttribute="viewtask" action="/admin/view">

    ID:<form:input path="id"  type="text" readonly="true"/><br/>

<%--    Employees:<form:input path="name" type="text" readonly="true"/><br/>--%>
<%--              <form:input path="surname" type="text" readonly="true"/><br/>--%>

    Title: <form:input path="title" type="text" readonly="true"/><br/>

    Description:<form:input path="description" type="text" readonly="true"/><br/>


    Start date: <form:input path="startDate" type="date" name="start_date" readonly="true"/><br/>


    End date: <form:input path="endDate" type="date" readonly="true"/><br/>

<%--    Name: <form:input path="userName" type="text" placeholder="name" readonly="true"/><br/>--%>

        <a type="button" style="color: darkred" href="/admin/taskboard" ><h2><input type="button" value="Back To List"></h2></a>


</form:form>
</body>
</html>
