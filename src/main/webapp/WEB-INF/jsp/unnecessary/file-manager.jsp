<%--
  Created by IntelliJ IDEA.
  User: nailbayramoglu
  Date: 24.11.19
  Time: 01:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en" dir="ltr">

<!-- Mirrored from puffintheme.com/craft/soccer/project/app-filemanager.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Nov 2019 08:08:36 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="icon" href="favicon.ico" type="image/x-icon"/>

    <title>:: Soccer :: App Filemanager</title>

    <!-- Bootstrap Core and vandor -->
    <link rel="stylesheet" href="../assets/plugins/bootstrap/css/bootstrap.min.css" />

    <!-- Core css -->
    <link rel="stylesheet" href="../assets/css/main.css"/>
    <link rel="stylesheet" href="../assets/css/theme1.css"/>
</head>

<body class="font-montserrat">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
    </div>
</div>
<!-- Overlay For Sidebars -->

<div id="main_content">

    <jsp:include page="../common/header-top.jsp"/>

    <div id="rightsidebar" class="right_sidebar">
        <a href="javascript:void(0)" class="p-3 settingbar float-right"><i class="fa fa-close"></i></a>
        <div class="p-4">
            <div class="mb-4">
                <h6 class="font-14 font-weight-bold text-muted">Font Style</h6>
                <div class="custom-controls-stacked font_setting">
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="font" value="font-opensans">
                        <span class="custom-control-label">Open Sans Font</span>
                    </label>
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="font" value="font-montserrat" checked="">
                        <span class="custom-control-label">Montserrat Google Font</span>
                    </label>
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="font" value="font-roboto">
                        <span class="custom-control-label">Robot Google Font</span>
                    </label>
                </div>
            </div>
            <hr>
            <div class="mb-4">
                <h6 class="font-14 font-weight-bold text-muted">Dropdown Menu Icon</h6>
                <div class="custom-controls-stacked arrow_option">
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="marrow" value="arrow-a">
                        <span class="custom-control-label">A</span>
                    </label>
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="marrow" value="arrow-b">
                        <span class="custom-control-label">B</span>
                    </label>
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="marrow" value="arrow-c" checked="">
                        <span class="custom-control-label">C</span>
                    </label>
                </div>
                <h6 class="font-14 font-weight-bold mt-4 text-muted">SubMenu List Icon</h6>
                <div class="custom-controls-stacked list_option">
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="listicon" value="list-a" checked="">
                        <span class="custom-control-label">A</span>
                    </label>
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="listicon" value="list-b">
                        <span class="custom-control-label">B</span>
                    </label>
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="listicon" value="list-c">
                        <span class="custom-control-label">C</span>
                    </label>
                </div>
            </div>
            <hr>
            <div>
                <h6 class="font-14 font-weight-bold mt-4 text-muted">General Settings</h6>
                <ul class="setting-list list-unstyled mt-1 setting_switch">
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Night Mode</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-darkmode">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Fix Navbar top</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-fixnavbar">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Header Dark</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-pageheader" checked="">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Min Sidebar Dark</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-min_sidebar">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Sidebar Dark</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-sidebar">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Icon Color</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-iconcolor">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Gradient Color</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-gradient">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Box Shadow</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-boxshadow">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">RTL Support</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-rtl">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Box Layout</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-boxlayout">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                </ul>
            </div>
            <hr>
            <div class="form-group">
                <label class="d-block">Storage <span class="float-right">77%</span></label>
                <div class="progress progress-sm">
                    <div class="progress-bar" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 77%;"></div>
                </div>
                <button type="button" class="btn btn-primary btn-block mt-3">Upgrade Storage</button>
            </div>
        </div>
    </div>

    <div class="theme_div">
        <div class="card">
            <div class="card-body">
                <ul class="list-group list-unstyled">
                    <li class="list-group-item mb-2">
                        <p>Default Theme</p>
                        <a href="index-2.html"><img src="../assets/images/themes/default.png" class="img-fluid" /></a>
                    </li>
                    <li class="list-group-item mb-2">
                        <p>Night Mode Theme</p>
                        <a href="http://puffintheme.com/craft/soccer/project-dark/index.html"><img src="../assets/images/themes/dark.png" class="img-fluid" /></a>
                    </li>
                    <li class="list-group-item mb-2">
                        <p>RTL Version</p>
                        <a href="http://puffintheme.com/craft/soccer/project-rtl/index.html"><img src="../assets/images/themes/rtl.png" class="img-fluid" /></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="user_div">
        <jsp:include page="../common/brand-name.jsp"/>
        <div class="card-body">
            <a href="page-profile.html"><img class="card-profile-img" src="../assets/images/sm/avatar1.jpg" alt=""></a>
            <h6 class="mb-0">Peter Richards</h6>
            <span>peter.richard@gmail.com</span>
            <div class="d-flex align-items-baseline mt-3">
                <h3 class="mb-0 mr-2">9.8</h3>
                <p class="mb-0"><span class="text-success">1.6% <i class="fa fa-arrow-up"></i></span></p>
            </div>
            <div class="progress progress-xs">
                <div class="progress-bar" role="progressbar" style="width: 15%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar bg-info" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar bg-success" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar bg-orange" role="progressbar" style="width: 5%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar bg-indigo" role="progressbar" style="width: 13%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <h6 class="text-uppercase font-10 mt-1">Performance Score</h6>
            <hr>
            <p>Activity</p>
            <ul class="new_timeline">
                <li>
                    <div class="bullet pink"></div>
                    <div class="time">11:00am</div>
                    <div class="desc">
                        <h3>Attendance</h3>
                        <h4>Computer Class</h4>
                    </div>
                </li>
                <li>
                    <div class="bullet pink"></div>
                    <div class="time">11:30am</div>
                    <div class="desc">
                        <h3>Added an interest</h3>
                        <h4>“Volunteer Activities”</h4>
                    </div>
                </li>
                <li>
                    <div class="bullet green"></div>
                    <div class="time">12:00pm</div>
                    <div class="desc">
                        <h3>Developer Team</h3>
                        <h4>Hangouts</h4>
                        <ul class="list-unstyled team-info margin-0 p-t-5">
                            <li><img src="../assets/images/xs/avatar1.jpg" alt="Avatar"></li>
                            <li><img src="../assets/images/xs/avatar2.jpg" alt="Avatar"></li>
                            <li><img src="../assets/images/xs/avatar3.jpg" alt="Avatar"></li>
                            <li><img src="../assets/images/xs/avatar4.jpg" alt="Avatar"></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <div class="bullet green"></div>
                    <div class="time">2:00pm</div>
                    <div class="desc">
                        <h3>Responded to need</h3>
                        <a href="javascript:void(0)">“In-Kind Opportunity”</a>
                    </div>
                </li>
                <li>
                    <div class="bullet orange"></div>
                    <div class="time">1:30pm</div>
                    <div class="desc">
                        <h3>Lunch Break</h3>
                    </div>
                </li>
                <li>
                    <div class="bullet green"></div>
                    <div class="time">2:38pm</div>
                    <div class="desc">
                        <h3>Finish</h3>
                        <h4>Go to Home</h4>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <jsp:include page="../common/sidebar.jsp"/>

    <div class="page">
        <jsp:include page="../common/page-top.jsp"/>
        <div class="section-body mt-3">
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Recently Accessed Files</h3>
                                <div class="card-options">
                                    <a href="javascript:void(0)"><i class="fa fa-plus" data-toggle="tooltip" data-placement="right" title="Upload New"></i></a>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="file_folder">
                                    <a href="javascript:void(0);">
                                        <div class="icon">
                                            <i class="fa fa-folder text-success"></i>
                                        </div>
                                        <div class="file-name">
                                            <p class="mb-0 text-muted">Family</p>
                                            <small>3 File, 1.2GB</small>
                                        </div>
                                    </a>
                                    <a href="javascript:void(0);">
                                        <div class="icon">
                                            <i class="fa fa-file-word-o text-primary"></i>
                                        </div>
                                        <div class="file-name">
                                            <p class="mb-0 text-muted">Report2017.doc</p>
                                            <small>Size: 68KB</small>
                                        </div>
                                    </a>
                                    <a href="javascript:void(0);">
                                        <div class="icon">
                                            <i class="fa fa-file-pdf-o text-danger"></i>
                                        </div>
                                        <div class="file-name">
                                            <p class="mb-0 text-muted">Report2017.pdf</p>
                                            <small>Size: 68KB</small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card bg-none b-none">
                            <div class="card-body pt-0">
                                <div class="table-responsive">
                                    <table class="table table-hover table-vcenter table_custom text-nowrap spacing5 text-nowrap mb-0">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>Name</th>
                                            <th>Share With</th>
                                            <th>Owner</th>
                                            <th>Last Update</th>
                                            <th>File Size</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="width45">
                                                <i class="fa fa-folder"></i>
                                            </td>
                                            <td>
                                                <span class="folder-name">Work</span>
                                            </td>
                                            <td>
                                                <div class="avatar-list avatar-list-stacked">
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar1.jpg" data-toggle="tooltip" title="Avatar"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar2.jpg" data-toggle="tooltip" title="Avatar"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar3.jpg" data-toggle="tooltip" title="Avatar"/>
                                                </div>
                                            </td>
                                            <td class="width100">
                                                <span>Me</span>
                                            </td>
                                            <td class="width100">
                                                <span>Oct 7, 2018</span>
                                            </td>
                                            <td class="width100 text-center">
                                                <span class="size"> - </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="width45">
                                                <i class="fa fa-folder"></i>
                                            </td>
                                            <td>
                                                <span class="folder-name">Family</span>
                                            </td>
                                            <td>
                                                -
                                            </td>
                                            <td class="width100">
                                                <span>Me</span>
                                            </td>
                                            <td class="width100">
                                                <span>Oct 17, 2018</span>
                                            </td>
                                            <td class="width100 text-center">
                                                <span class="size"> - </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="width45">
                                                <i class="fa fa-folder text-danger"></i>
                                            </td>
                                            <td>
                                                <span class="folder-name">Holidays</span>
                                            </td>
                                            <td>
                                                -
                                            </td>
                                            <td class="width100">
                                                <span>John</span>
                                            </td>
                                            <td class="width100">
                                                <span>Oct 18, 2018</span>
                                            </td>
                                            <td class="width100 text-center">
                                                <span class="size"> 50MB </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="width45">
                                                <i class="fa fa-folder"></i>
                                            </td>
                                            <td>
                                                <span class="folder-name">Mobile Work </span>
                                            </td>
                                            <td>
                                                -
                                            </td>
                                            <td class="width100">
                                                <span>Me</span>
                                            </td>
                                            <td class="width100">
                                                <span>April 7, 2019</span>
                                            </td>
                                            <td class="width100 text-center">
                                                <span class="size"> 1GB </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="width45">
                                                <i class="fa fa-folder"></i>
                                            </td>
                                            <td>
                                                <span class="folder-name">Photoshop Data</span>
                                            </td>
                                            <td>
                                                <div class="avatar-list avatar-list-stacked">
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar1.jpg" data-toggle="tooltip" title="Avatar"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar2.jpg" data-toggle="tooltip" title="Avatar"/>
                                                </div>
                                            </td>
                                            <td class="width100">
                                                <span>Andrew</span>
                                            </td>
                                            <td class="width100">
                                                <span>Nov 23, 2019</span>
                                            </td>
                                            <td class="width100 text-center">
                                                <span class="size"> - </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="width45">
                                                <i class="fa fa-folder text-danger"></i>
                                            </td>
                                            <td>
                                                <span class="folder-name">Holidays</span>
                                            </td>
                                            <td>
                                                <div class="avatar-list avatar-list-stacked">
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar5.jpg" data-toggle="tooltip" title="Avatar"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar6.jpg" data-toggle="tooltip" title="Avatar"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar1.jpg" data-toggle="tooltip" title="Avatar"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar4.jpg" data-toggle="tooltip" title="Avatar"/>
                                                </div>
                                            </td>
                                            <td class="width100">
                                                <span>Me</span>
                                            </td>
                                            <td class="width100">
                                                <span>Dec 5, 2018</span>
                                            </td>
                                            <td class="width100 text-center">
                                                <span class="size"> 100MB </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="width45">
                                                <i class="fa fa-file-excel-o text-success"></i>
                                            </td>
                                            <td>
                                                <span class="folder-name">new timesheet.xlsx</span>
                                            </td>
                                            <td>
                                                -
                                            </td>
                                            <td class="width100">
                                                <span>Me</span>
                                            </td>
                                            <td class="width100">
                                                <span>Dec 5, 2018</span>
                                            </td>
                                            <td class="width100 text-center">
                                                <span class="size"> 52KB </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="width45">
                                                <i class="fa fa-file-word-o text-warning"></i>
                                            </td>
                                            <td>
                                                <span class="folder-name">new project.doc</span>
                                            </td>
                                            <td>
                                                -
                                            </td>
                                            <td class="width100">
                                                <span>Me</span>
                                            </td>
                                            <td class="width100">
                                                <span>May 5, 2019</span>
                                            </td>
                                            <td class="width100 text-center">
                                                <span class="size"> 152KB </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="width45">
                                                <i class="fa fa-file-pdf-o text-warning"></i>
                                            </td>
                                            <td>
                                                <span class="folder-name">report.pdf</span>
                                            </td>
                                            <td>
                                                -
                                            </td>
                                            <td class="width100">
                                                <span>Me</span>
                                            </td>
                                            <td class="width100">
                                                <span>May 2, 2019</span>
                                            </td>
                                            <td class="width100 text-center">
                                                <span class="size"> 841KB </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="width45">
                                                <i class="fa fa-file-pdf-o text-warning"></i>
                                            </td>
                                            <td>
                                                <span class="folder-name">report-2018.pdf</span>
                                            </td>
                                            <td>
                                                -
                                            </td>
                                            <td class="width100">
                                                <span>Me</span>
                                            </td>
                                            <td class="width100">
                                                <span>Oct 16, 2018</span>
                                            </td>
                                            <td class="width100 text-center">
                                                <span class="size"> 541KB </span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="../assets/bundles/lib.vendor.bundle.js"></script>

<script src="../assets/js/core.js"></script>
</body>

<!-- Mirrored from puffintheme.com/craft/soccer/project/app-filemanager.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Nov 2019 08:08:36 GMT -->
</html>