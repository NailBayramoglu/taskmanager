<%--
  Created by IntelliJ IDEA.
  User: malik
  Date: 11/10/19
  Time: 1:52 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Task Scheduler</title>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
    <style>
        body {
            margin: 0;
            padding: 0;
            background-color: #f1f1f1;
        }

        .box {
            width: 1270px;
            padding: 20px;
            background-color: #fff;
            border: 1px solid #ccc;
            border-radius: 5px;
            margin-top: 25px;
            box-sizing: border-box;
        }
    </style>
</head>
<body>


<div class="container box">
    <div>
        <a href="add" class="btn btn-primary">Add Task</a>
    </div>
    <br>
    <div class="table-responsive">
        <table id="task_table" class="table table-bordered table-striped">
            <thead>
            <tr>
                <td>ID</td>
                <td>Task Name</td>
                <td>Description</td>
                <td>Start Date</td>
                <td>End Date</td>
                <td>Person</td>
                <td>Insert date</td>
                <td>Last Update</td>
                <td>Action</td>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
</body>
<script>
    $(document).ready(function () {

        fetch_data();

        function fetch_data() {
            $('#task_table').DataTable({
                "lengthMenu": [[5, 10, 20, 50, 100, -1], [5, 10, 20, 50, 100, "All"]],
                "processing": true,
                "serverSide": true,
                "ajax": "/list"
            });
        }

        $(document).on('click', '.delete', function () {
            var id = $(this).attr("id");
            if (confirm("are you sure you want to remove task?")) {
                $.ajax({
                    url: "delete",
                    method: "post",
                    data: {id: id},
                    success: function (data) {
                        $('#alert_message').html('<div class="alert alert-success">' + data + '</div>');
                        $('#task_table').DataTable().destroy();
                        fetch_data();
                    }
                });
                setInterval(function () {
                    $('#alert_message').html('');
                }, 100);
            }
        });

    });
</script>

</html>