<%--
  Created by IntelliJ IDEA.
  User: nailbayramoglu
  Date: 24.11.19
  Time: 00:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en" dir="ltr">

<!-- Mirrored from puffintheme.com/craft/soccer/project/project-list.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Nov 2019 08:08:41 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="icon" href="favicon.ico" type="image/x-icon"/>

    <title>:: Soccer :: Project List</title>

    <!-- Bootstrap Core and vandor -->
    <link rel="stylesheet" href="../assets/plugins/bootstrap/css/bootstrap.min.css" />

    <!-- Core css -->
    <link rel="stylesheet" href="../assets/css/main.css"/>
    <link rel="stylesheet" href="../assets/css/theme1.css"/>
</head>

<body class="font-montserrat">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
    </div>
</div>

<div id="main_content">

    <jsp:include page="../common/header-top.jsp"/>

    <div id="rightsidebar" class="right_sidebar">
        <a href="javascript:void(0)" class="p-3 settingbar float-right"><i class="fa fa-close"></i></a>
        <div class="p-4">
            <div class="mb-4">
                <h6 class="font-14 font-weight-bold text-muted">Font Style</h6>
                <div class="custom-controls-stacked font_setting">
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="font" value="font-opensans">
                        <span class="custom-control-label">Open Sans Font</span>
                    </label>
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="font" value="font-montserrat" checked="">
                        <span class="custom-control-label">Montserrat Google Font</span>
                    </label>
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="font" value="font-roboto">
                        <span class="custom-control-label">Robot Google Font</span>
                    </label>
                </div>
            </div>
            <hr>
            <div class="mb-4">
                <h6 class="font-14 font-weight-bold text-muted">Dropdown Menu Icon</h6>
                <div class="custom-controls-stacked arrow_option">
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="marrow" value="arrow-a">
                        <span class="custom-control-label">A</span>
                    </label>
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="marrow" value="arrow-b">
                        <span class="custom-control-label">B</span>
                    </label>
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="marrow" value="arrow-c" checked="">
                        <span class="custom-control-label">C</span>
                    </label>
                </div>
                <h6 class="font-14 font-weight-bold mt-4 text-muted">SubMenu List Icon</h6>
                <div class="custom-controls-stacked list_option">
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="listicon" value="list-a" checked="">
                        <span class="custom-control-label">A</span>
                    </label>
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="listicon" value="list-b">
                        <span class="custom-control-label">B</span>
                    </label>
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="listicon" value="list-c">
                        <span class="custom-control-label">C</span>
                    </label>
                </div>
            </div>
            <hr>
            <div>
                <h6 class="font-14 font-weight-bold mt-4 text-muted">General Settings</h6>
                <ul class="setting-list list-unstyled mt-1 setting_switch">
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Night Mode</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-darkmode">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Fix Navbar top</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-fixnavbar">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Header Dark</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-pageheader" checked="">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Min Sidebar Dark</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-min_sidebar">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Sidebar Dark</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-sidebar">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Icon Color</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-iconcolor">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Gradient Color</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-gradient">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Box Shadow</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-boxshadow">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">RTL Support</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-rtl">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                    <li>
                        <label class="custom-switch">
                            <span class="custom-switch-description">Box Layout</span>
                            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input btn-boxlayout">
                            <span class="custom-switch-indicator"></span>
                        </label>
                    </li>
                </ul>
            </div>
            <hr>
            <div class="form-group">
                <label class="d-block">Storage <span class="float-right">77%</span></label>
                <div class="progress progress-sm">
                    <div class="progress-bar" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 77%;"></div>
                </div>
                <button type="button" class="btn btn-primary btn-block mt-3">Upgrade Storage</button>
            </div>
        </div>
    </div>

    <div class="theme_div">
        <div class="card">
            <div class="card-body">
                <ul class="list-group list-unstyled">
                    <li class="list-group-item mb-2">
                        <p>Default Theme</p>
                        <a href="index-2.html"><img src="../assets/images/themes/default.png" class="img-fluid" /></a>
                    </li>
                    <li class="list-group-item mb-2">
                        <p>Night Mode Theme</p>
                        <a href="http://puffintheme.com/craft/soccer/project-dark/index.html"><img src="../assets/images/themes/dark.png" class="img-fluid" /></a>
                    </li>
                    <li class="list-group-item mb-2">
                        <p>RTL Version</p>
                        <a href="http://puffintheme.com/craft/soccer/project-rtl/index.html"><img src="../assets/images/themes/rtl.png" class="img-fluid" /></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="user_div">
        <jsp:include page="../common/brand-name.jsp"/>
        <div class="card-body">
            <a href="page-profile.html"><img class="card-profile-img" src="../assets/images/sm/avatar1.jpg" alt=""></a>
            <h6 class="mb-0">Peter Richards</h6>
            <span>peter.richard@gmail.com</span>
            <div class="d-flex align-items-baseline mt-3">
                <h3 class="mb-0 mr-2">9.8</h3>
                <p class="mb-0"><span class="text-success">1.6% <i class="fa fa-arrow-up"></i></span></p>
            </div>
            <div class="progress progress-xs">
                <div class="progress-bar" role="progressbar" style="width: 15%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar bg-info" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar bg-success" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar bg-orange" role="progressbar" style="width: 5%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar bg-indigo" role="progressbar" style="width: 13%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <h6 class="text-uppercase font-10 mt-1">Performance Score</h6>
            <hr>
            <p>Activity</p>
            <ul class="new_timeline">
                <li>
                    <div class="bullet pink"></div>
                    <div class="time">11:00am</div>
                    <div class="desc">
                        <h3>Attendance</h3>
                        <h4>Computer Class</h4>
                    </div>
                </li>
                <li>
                    <div class="bullet pink"></div>
                    <div class="time">11:30am</div>
                    <div class="desc">
                        <h3>Added an interest</h3>
                        <h4>“Volunteer Activities”</h4>
                    </div>
                </li>
                <li>
                    <div class="bullet green"></div>
                    <div class="time">12:00pm</div>
                    <div class="desc">
                        <h3>Developer Team</h3>
                        <h4>Hangouts</h4>
                        <ul class="list-unstyled team-info margin-0 p-t-5">
                            <li><img src="../assets/images/xs/avatar1.jpg" alt="Avatar"></li>
                            <li><img src="../assets/images/xs/avatar2.jpg" alt="Avatar"></li>
                            <li><img src="../assets/images/xs/avatar3.jpg" alt="Avatar"></li>
                            <li><img src="../assets/images/xs/avatar4.jpg" alt="Avatar"></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <div class="bullet green"></div>
                    <div class="time">2:00pm</div>
                    <div class="desc">
                        <h3>Responded to need</h3>
                        <a href="javascript:void(0)">“In-Kind Opportunity”</a>
                    </div>
                </li>
                <li>
                    <div class="bullet orange"></div>
                    <div class="time">1:30pm</div>
                    <div class="desc">
                        <h3>Lunch Break</h3>
                    </div>
                </li>
                <li>
                    <div class="bullet green"></div>
                    <div class="time">2:38pm</div>
                    <div class="desc">
                        <h3>Finish</h3>
                        <h4>Go to Home</h4>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <jsp:include page="../common/sidebar.jsp"/>

    <div class="page">
       <jsp:include page="../common/page-top.jsp"/>
        <div class="section-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="d-flex justify-content-between align-items-center">
                            <ul class="nav nav-tabs page-header-tab">
                                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#Project-OnGoing">OnGoing</a></li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Project-UpComing">UpComing</a></li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Project-add">Add</a></li>
                            </ul>
                            <div class="header-action d-md-flex">
                                <div class="input-group mr-2">
                                    <input type="text" class="form-control" placeholder="Search...">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-body mt-3">
            <div class="container-fluid">
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="Project-OnGoing" role="tabpanel">
                        <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">New Admin Design</h3>
                                        <div class="card-options">
                                            <label class="custom-switch m-0">
                                                <input type="checkbox" value="1" class="custom-switch-input" checked>
                                                <span class="custom-switch-indicator"></span>
                                            </label>
                                            <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <span class="tag tag-blue mb-3">Web Design</span>
                                        <p>Aperiam deleniti fugit incidunt, iste, itaque minima neque pariatur perferendis temporibus!</p>
                                        <div class="row">
                                            <div class="col-5 py-1"><strong>Created:</strong></div>
                                            <div class="col-7 py-1">09 Jun 2019 11:32AM</div>
                                            <div class="col-5 py-1"><strong>Creator:</strong></div>
                                            <div class="col-7 py-1">Nathan Guerrero</div>
                                            <div class="col-5 py-1"><strong>Question:</strong></div>
                                            <div class="col-7 py-1"><strong>23</strong></div>
                                            <div class="col-5 py-1"><strong>Comments:</strong></div>
                                            <div class="col-7 py-1"><strong>32</strong></div>
                                            <div class="col-5 py-1"><strong>Bug:</strong></div>
                                            <div class="col-7 py-1"><strong>5</strong></div>
                                            <div class="col-5 py-1"><strong>Team:</strong></div>
                                            <div class="col-7 py-1">
                                                <div class="avatar-list avatar-list-stacked">
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar1.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar2.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar3.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar4.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar5.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <span class="avatar avatar-sm">+8</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="clearfix">
                                            <div class="float-left"><strong>15%</strong></div>
                                            <div class="float-right"><small class="text-muted">Progress</small></div>
                                        </div>
                                        <div class="progress progress-xs">
                                            <div class="progress-bar bg-red" role="progressbar" style="width: 15%" aria-valuenow="36" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Job Portal Web App</h3>
                                        <div class="card-options">
                                            <label class="custom-switch m-0">
                                                <input type="checkbox" value="1" class="custom-switch-input" checked>
                                                <span class="custom-switch-indicator"></span>
                                            </label>
                                            <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <span class="tag tag-pink mb-3">Angular</span>
                                        <p>Aperiam deleniti fugit incidunt, iste, itaque minima neque pariatur perferendis temporibus!</p>
                                        <div class="row">
                                            <div class="col-5 py-1"><strong>Created:</strong></div>
                                            <div class="col-7 py-1">09 Jun 2019 11:32AM</div>
                                            <div class="col-5 py-1"><strong>Creator:</strong></div>
                                            <div class="col-7 py-1">Nathan Guerrero</div>
                                            <div class="col-5 py-1"><strong>Question:</strong></div>
                                            <div class="col-7 py-1"><strong>55</strong></div>
                                            <div class="col-5 py-1"><strong>Comments:</strong></div>
                                            <div class="col-7 py-1"><strong>43</strong></div>
                                            <div class="col-5 py-1"><strong>Bug:</strong></div>
                                            <div class="col-7 py-1"><strong>5</strong></div>
                                            <div class="col-5 py-1"><strong>Team:</strong></div>
                                            <div class="col-7 py-1">
                                                <div class="avatar-list avatar-list-stacked">
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar6.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar7.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar8.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar1.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar2.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <span class="avatar avatar-sm">+8</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="clearfix">
                                            <div class="float-left"><strong>75%</strong></div>
                                            <div class="float-right"><small class="text-muted">Progress</small></div>
                                        </div>
                                        <div class="progress progress-xs">
                                            <div class="progress-bar bg-green" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">App design and Development</h3>
                                        <div class="card-options">
                                            <label class="custom-switch m-0">
                                                <input type="checkbox" value="1" class="custom-switch-input" checked>
                                                <span class="custom-switch-indicator"></span>
                                            </label>
                                            <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <span class="tag tag-green mb-3">Android</span>
                                        <p>Aperiam deleniti fugit incidunt, iste, itaque minima neque pariatur perferendis temporibus!</p>
                                        <div class="row">
                                            <div class="col-5 py-1"><strong>Created:</strong></div>
                                            <div class="col-7 py-1">09 Jun 2019 11:32AM</div>
                                            <div class="col-5 py-1"><strong>Creator:</strong></div>
                                            <div class="col-7 py-1">Nathan Guerrero</div>
                                            <div class="col-5 py-1"><strong>Question:</strong></div>
                                            <div class="col-7 py-1"><strong>12</strong></div>
                                            <div class="col-5 py-1"><strong>Comments:</strong></div>
                                            <div class="col-7 py-1"><strong>96</strong></div>
                                            <div class="col-5 py-1"><strong>Bug:</strong></div>
                                            <div class="col-7 py-1"><strong>7</strong></div>
                                            <div class="col-5 py-1"><strong>Team:</strong></div>
                                            <div class="col-7 py-1">
                                                <div class="avatar-list avatar-list-stacked">
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar1.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar2.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar5.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <span class="avatar avatar-sm">+8</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="clearfix">
                                            <div class="float-left"><strong>47%</strong></div>
                                            <div class="float-right"><small class="text-muted">Progress</small></div>
                                        </div>
                                        <div class="progress progress-xs">
                                            <div class="progress-bar bg-blue" role="progressbar" style="width: 47%" aria-valuenow="47" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Job Portal Web App</h3>
                                        <div class="card-options">
                                            <label class="custom-switch m-0">
                                                <input type="checkbox" value="1" class="custom-switch-input" checked>
                                                <span class="custom-switch-indicator"></span>
                                            </label>
                                            <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <span class="tag tag-pink mb-3">Angular</span>
                                        <p>Aperiam deleniti fugit incidunt, iste, itaque minima neque pariatur perferendis temporibus!</p>
                                        <div class="row">
                                            <div class="col-5 py-1"><strong>Created:</strong></div>
                                            <div class="col-7 py-1">09 Jun 2019 11:32AM</div>
                                            <div class="col-5 py-1"><strong>Creator:</strong></div>
                                            <div class="col-7 py-1">Nathan Guerrero</div>
                                            <div class="col-5 py-1"><strong>Question:</strong></div>
                                            <div class="col-7 py-1"><strong>55</strong></div>
                                            <div class="col-5 py-1"><strong>Comments:</strong></div>
                                            <div class="col-7 py-1"><strong>43</strong></div>
                                            <div class="col-5 py-1"><strong>Bug:</strong></div>
                                            <div class="col-7 py-1"><strong>5</strong></div>
                                            <div class="col-5 py-1"><strong>Team:</strong></div>
                                            <div class="col-7 py-1">
                                                <div class="avatar-list avatar-list-stacked">
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar6.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar7.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar8.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar1.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar2.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <span class="avatar avatar-sm">+8</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="clearfix">
                                            <div class="float-left"><strong>75%</strong></div>
                                            <div class="float-right"><small class="text-muted">Progress</small></div>
                                        </div>
                                        <div class="progress progress-xs">
                                            <div class="progress-bar bg-green" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">One Page landing</h3>
                                        <div class="card-options">
                                            <label class="custom-switch m-0">
                                                <input type="checkbox" value="1" class="custom-switch-input" checked>
                                                <span class="custom-switch-indicator"></span>
                                            </label>
                                            <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <span class="tag tag-blue mb-3">Wordpress</span>
                                        <p>Aperiam deleniti fugit incidunt, iste, itaque minima neque pariatur perferendis temporibus!</p>
                                        <div class="row">
                                            <div class="col-5 py-1"><strong>Created:</strong></div>
                                            <div class="col-7 py-1">09 Jun 2019 11:32AM</div>
                                            <div class="col-5 py-1"><strong>Creator:</strong></div>
                                            <div class="col-7 py-1">Nathan Guerrero</div>
                                            <div class="col-5 py-1"><strong>Question:</strong></div>
                                            <div class="col-7 py-1"><strong>23</strong></div>
                                            <div class="col-5 py-1"><strong>Comments:</strong></div>
                                            <div class="col-7 py-1"><strong>32</strong></div>
                                            <div class="col-5 py-1"><strong>Bug:</strong></div>
                                            <div class="col-7 py-1"><strong>5</strong></div>
                                            <div class="col-5 py-1"><strong>Team:</strong></div>
                                            <div class="col-7 py-1">
                                                <div class="avatar-list avatar-list-stacked">
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar1.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar2.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar3.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar4.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar5.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <span class="avatar avatar-sm">+8</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="clearfix">
                                            <div class="float-left"><strong>17%</strong></div>
                                            <div class="float-right"><small class="text-muted">Progress</small></div>
                                        </div>
                                        <div class="progress progress-xs">
                                            <div class="progress-bar bg-red" role="progressbar" style="width: 17%" aria-valuenow="36" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Job Portal Web App</h3>
                                        <div class="card-options">
                                            <label class="custom-switch m-0">
                                                <input type="checkbox" value="1" class="custom-switch-input" checked>
                                                <span class="custom-switch-indicator"></span>
                                            </label>
                                            <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <span class="tag tag-gray mb-3">iOS App</span>
                                        <p>Aperiam deleniti fugit incidunt, iste, itaque minima neque pariatur perferendis temporibus!</p>
                                        <div class="row">
                                            <div class="col-5 py-1"><strong>Created:</strong></div>
                                            <div class="col-7 py-1">09 Jun 2019 11:32AM</div>
                                            <div class="col-5 py-1"><strong>Creator:</strong></div>
                                            <div class="col-7 py-1">Nathan Guerrero</div>
                                            <div class="col-5 py-1"><strong>Question:</strong></div>
                                            <div class="col-7 py-1"><strong>55</strong></div>
                                            <div class="col-5 py-1"><strong>Comments:</strong></div>
                                            <div class="col-7 py-1"><strong>43</strong></div>
                                            <div class="col-5 py-1"><strong>Bug:</strong></div>
                                            <div class="col-7 py-1"><strong>5</strong></div>
                                            <div class="col-5 py-1"><strong>Team:</strong></div>
                                            <div class="col-7 py-1">
                                                <div class="avatar-list avatar-list-stacked">
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar6.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar7.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar8.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar1.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <img class="avatar avatar-sm" src="../assets/images/xs/avatar2.jpg" data-toggle="tooltip" title="" data-original-title="Avatar Name"/>
                                                    <span class="avatar avatar-sm">+8</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="clearfix">
                                            <div class="float-left"><strong>81%</strong></div>
                                            <div class="float-right"><small class="text-muted">Progress</small></div>
                                        </div>
                                        <div class="progress progress-xs">
                                            <div class="progress-bar bg-green" role="progressbar" style="width: 81%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="Project-UpComing" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover table-striped table-vcenter mb-0 text-nowrap">
                                                <thead>
                                                <tr>
                                                    <th>Owner</th>
                                                    <th>Milestone</th>
                                                    <th class="w100">Work</th>
                                                    <th class="w100">Duration</th>
                                                    <th>Priority</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td><img src="../assets/images/xs/avatar1.jpg" alt="Avatar" class="w30 rounded-circle mr-2"> <span>Isidore Dilao</span></td>
                                                    <td>Account receivable</td>
                                                    <td><span>30:00</span></td>
                                                    <td>30:0 hrs</td>
                                                    <td><span class="text-warning">Medium</span></td>
                                                </tr>
                                                <tr>
                                                    <td><img src="../assets/images/xs/avatar2.jpg" alt="Avatar" class="w30 rounded-circle mr-2"> <span>Maricel Villalon</span></td>
                                                    <td>Account receivable</td>
                                                    <td><span>68:00</span></td>
                                                    <td>105:0 hrs</td>
                                                    <td><span class="text-danger">High</span></td>
                                                </tr>
                                                <tr>
                                                    <td><img src="../assets/images/xs/avatar3.jpg" alt="Avatar" class="w30 rounded-circle mr-2"> <span>Theresa Wright</span></td>
                                                    <td>Approval site</td>
                                                    <td><span>74:00</span></td>
                                                    <td>89:0 hrs</td>
                                                    <td><span>None</span></td>
                                                </tr>
                                                <tr>
                                                    <td><img src="../assets/images/xs/avatar4.jpg" alt="Avatar" class="w30 rounded-circle mr-2"> <span>Jason Porter</span></td>
                                                    <td>Final touch up</td>
                                                    <td><span>30:00</span></td>
                                                    <td>30:0 hrs</td>
                                                    <td><span>None</span></td>
                                                </tr>
                                                <tr>
                                                    <td><img src="../assets/images/xs/avatar5.jpg" alt="Avatar" class="w30 rounded-circle mr-2"> <span>Annelyn Mercado</span></td>
                                                    <td>Account receivable</td>
                                                    <td><span>30:00</span></td>
                                                    <td>30:0 hrs</td>
                                                    <td><span>None</span></td>
                                                </tr>
                                                <tr>
                                                    <td><img src="../assets/images/xs/avatar6.jpg" alt="Avatar" class="w30 rounded-circle mr-2"> <span>Sean Black</span></td>
                                                    <td>Basement slab preparation</td>
                                                    <td><span>88:00</span></td>
                                                    <td>88:0 hrs</td>
                                                    <td><span>None</span></td>
                                                </tr>
                                                <tr>
                                                    <td><img src="../assets/images/xs/avatar7.jpg" alt="Avatar" class="w30 rounded-circle mr-2"> <span>Scott Ortega</span></td>
                                                    <td>Account receivable</td>
                                                    <td><span>56:00</span></td>
                                                    <td>125:0 hrs</td>
                                                    <td><span class="text-warning">Medium</span></td>
                                                </tr>
                                                <tr>
                                                    <td><img src="../assets/images/xs/avatar8.jpg" alt="Avatar" class="w30 rounded-circle mr-2"> <span>David Wallace</span></td>
                                                    <td>Account receivable</td>
                                                    <td><span>30:00</span></td>
                                                    <td>30:0 hrs</td>
                                                    <td><span>None</span></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="Project-add" role="tabpanel">

                    </div>
                </div>
            </div>
        </div>
        <div class="section-body">
            <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            Copyright © 2019 <a href="https://themeforest.net/user/admincraft/portfolio">admincraft</a>.
                        </div>
                        <div class="col-md-6 col-sm-12 text-md-right">
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item"><a href="http://puffintheme.com/craft/soccer/doc/index.html">Documentation</a></li>
                                <li class="list-inline-item"><a href="javascript:void(0)">FAQ</a></li>
                                <li class="list-inline-item"><a href="javascript:void(0)" class="btn btn-outline-primary btn-icon">Buy Now</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>


<script src="../assets/bundles/lib.vendor.bundle.js"></script>

<script src="../assets/js/core.js"></script>
</body>

<!-- Mirrored from puffintheme.com/craft/soccer/project/project-list.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Nov 2019 08:08:41 GMT -->
</html>
