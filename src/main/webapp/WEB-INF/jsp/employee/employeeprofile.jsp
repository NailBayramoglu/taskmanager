<%--
  Created by IntelliJ IDEA.
  User: nailbayramoglu
  Date: 11.12.19
  Time: 23:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en" dir="ltr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="icon" href="favicon.ico" type="image/x-icon"/>

    <title>Employee Profile</title>

    <!-- Bootstrap Core and vandor -->
    <link rel="stylesheet" href="../assets/plugins/bootstrap/css/bootstrap.min.css" />


    <link rel="stylesheet" href="../assets/plugins/summernote/dist/summernote.css"/>
    <link rel="stylesheet" href="../assets/plugins/fullcalendar/fullcalendar.min.css">

    <!-- Core css -->
    <link rel="stylesheet" href="../assets/css/main.css"/>
    <link rel="stylesheet" href="../assets/css/theme1.css"/>
</head>

<body class="font-montserrat">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
    </div>
</div>

<div id="main_content">

<jsp:include page="employee.common/header-top.jsp"/>
<jsp:include page="employee.common/sidebar.jsp"/>
    <div class="theme_div">
        <div class="card">
            <div class="card-body">
                <ul class="list-group list-unstyled">
                    <li class="list-group-item mb-2">
                        <p>Default Theme</p>
                        <a href="index-2.html"><img src="../assets/images/themes/default.png" class="img-fluid" /></a>
                    </li>
                    <li class="list-group-item mb-2">
                        <p>Night Mode Theme</p>
                        <a href="http://puffintheme.com/craft/soccer/project-dark/index.html"><img src="../assets/images/themes/dark.png" class="img-fluid" /></a>
                    </li>
                    <li class="list-group-item mb-2">
                        <p>RTL Version</p>
                        <a href="http://puffintheme.com/craft/soccer/project-rtl/index.html"><img src="../assets/images/themes/rtl.png" class="img-fluid" /></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="user_div">
        <h5 class="brand-name mb-4">Soccer<a href="javascript:void(0)" class="user_btn"><i class="icon-logout"></i></a></h5>
        <div class="card-body">
            <a href="page-profile.html"><img class="card-profile-img" src="../assets/images/sm/avatar1.jpg" alt=""></a>
            <h6 class="mb-0">Peter Richards</h6>
            <span>peter.richard@gmail.com</span>
            <div class="d-flex align-items-baseline mt-3">
                <h3 class="mb-0 mr-2">9.8</h3>
                <p class="mb-0"><span class="text-success">1.6% <i class="fa fa-arrow-up"></i></span></p>
            </div>
            <div class="progress progress-xs">
                <div class="progress-bar" role="progressbar" style="width: 15%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar bg-info" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar bg-success" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar bg-orange" role="progressbar" style="width: 5%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar bg-indigo" role="progressbar" style="width: 13%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <h6 class="text-uppercase font-10 mt-1">Performance Score</h6>
            <hr>
            <p>Activity</p>
            <ul class="new_timeline">
                <li>
                    <div class="bullet pink"></div>
                    <div class="time">11:00am</div>
                    <div class="desc">
                        <h3>Attendance</h3>
                        <h4>Computer Class</h4>
                    </div>
                </li>
                <li>
                    <div class="bullet pink"></div>
                    <div class="time">11:30am</div>
                    <div class="desc">
                        <h3>Added an interest</h3>
                        <h4>“Volunteer Activities”</h4>
                    </div>
                </li>
                <li>
                    <div class="bullet green"></div>
                    <div class="time">12:00pm</div>
                    <div class="desc">
                        <h3>Developer Team</h3>
                        <h4>Hangouts</h4>
                        <ul class="list-unstyled team-info margin-0 p-t-5">
                            <li><img src="../assets/images/xs/avatar1.jpg" alt="Avatar"></li>
                            <li><img src="../assets/images/xs/avatar2.jpg" alt="Avatar"></li>
                            <li><img src="../assets/images/xs/avatar3.jpg" alt="Avatar"></li>
                            <li><img src="../assets/images/xs/avatar4.jpg" alt="Avatar"></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <div class="bullet green"></div>
                    <div class="time">2:00pm</div>
                    <div class="desc">
                        <h3>Responded to need</h3>
                        <a href="javascript:void(0)">“In-Kind Opportunity”</a>
                    </div>
                </li>
                <li>
                    <div class="bullet orange"></div>
                    <div class="time">1:30pm</div>
                    <div class="desc">
                        <h3>Lunch Break</h3>
                    </div>
                </li>
                <li>
                    <div class="bullet green"></div>
                    <div class="time">2:38pm</div>
                    <div class="desc">
                        <h3>Finish</h3>
                        <h4>Go to Home</h4>
                    </div>
                </li>
            </ul>
        </div>
    </div>

<jsp:include page="employee.common/brand-name.jsp"/>
    <div class="page">
<jsp:include page="employee.common/page-top.jsp"/>

        <div class="section-body mt-4">

            <div class="container-fluid">
                <div class="row clearfix">
<%--                    <c:choose>--%>
<%--                    <c:when test="${ not empty employeeList}">--%>
<%--                    <c:forEach items="${employeeList}" var="emp" >--%>
                    <div class="col-lg-4 col-md-12">
                        <div class="card">
                            <img class="card-img-top" src="../assets/images/gallery/6.jpg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title">${sessionScope.user.name}&nbsp;${sessionScope.user.surname}</h5>
                                <p class="card-text">${sessionScope.user.department}</p>
                            </div>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">${sessionScope.user.position}</li>
                                <li class="list-group-item">${sessionScope.user.email}</li>
                                <li class="list-group-item">${sessionScope.user.birthDate}</li>
                                <li class="list-group-item">${sessionScope.user.mobile}</li>
                            </ul>
                        </div><br/>
                        </div>
                        <ul class="nav nav-tabs mb-3" id="pills-tab" role="tablist">
<%--                            <li class="nav-item">--%>
<%--                                <a class="nav-link active" id="pills-blog-tab" data-toggle="pill" href="#pills-blog" role="tab" aria-controls="pills-blog" aria-selected="true">Blog</a>--%>
<%--                            </li>--%>
<%--                            <li class="nav-item">--%>
<%--                                <a class="nav-link" id="pills-timeline-tab" data-toggle="pill" href="#pills-timeline" role="tab" aria-controls="pills-timeline" aria-selected="true">Timeline</a>--%>
<%--                            </li>--%>
<%--                            <li class="nav-item">--%>
<%--                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Profile</a>--%>
<%--                            </li>--%>
                        </ul>
<%--                        <div class="tab-content" id="pills-tabContent">--%>
<%--                            <div class="card">--%>
<%--                                <div class="card-body">--%>
<%--                                    <div class="row clearfix">--%>
<%--                                        <div class="col-md-5">--%>
<%--                                            <div class="form-group">--%>
<%--                                                <label class="form-label">Name</label>--%>
<%--                                                <input type="text" class="form-control"  readonly="true">--%>
<%--                                            </div>--%>
<%--                                        </div>--%>
<%--                                        <div class="col-sm-6 col-md-3">--%>
<%--                                            <div class="form-group">--%>
<%--                                                <label class="form-label">Surname</label>--%>
<%--                                                <input type="text" class="form-control" placeholder="Username">--%>
<%--                                            </div>--%>
<%--                                        </div>--%>
<%--                                        <div class="col-sm-6 col-md-3">--%>
<%--                                            <div class="form-group">--%>
<%--                                                <label class="form-label">Department</label>--%>
<%--                                                <input type="text" class="form-control" placeholder="Department" >--%>
<%--                                            </div>--%>
<%--                                        </div>--%>
<%--                                        <div class="col-sm-6 col-md-3">--%>
<%--                                            <div class="form-group">--%>
<%--                                                <label class="form-label">Position</label>--%>
<%--                                                <input type="text" class="form-control" placeholder="Position" >--%>
<%--                                            </div>--%>
<%--                                        </div>--%>
<%--                                        <div class="col-sm-6 col-md-4">--%>
<%--                                            <div class="form-group">--%>
<%--                                                <label class="form-label">Email address</label>--%>
<%--                                                <input type="email" class="form-control" placeholder="Email">--%>
<%--                                            </div>--%>
<%--                                        </div>--%>
<%--                                        <div class="col-sm-6 col-md-6">--%>
<%--                                            <div class="form-group">--%>
<%--                                                <label class="form-label">Phone number</label>--%>
<%--                                                <input type="text" class="form-control" placeholder="Phone" >--%>
<%--                                            </div>--%>
<%--                                        </div>--%>
<%--&lt;%&ndash;                                        <div class="col-sm-6 col-md-6">&ndash;%&gt;--%>
<%--&lt;%&ndash;                                            <div class="form-group">&ndash;%&gt;--%>
<%--&lt;%&ndash;                                                <label class="form-label">Tasks</label>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                                <input type="text" class="form-control" placeholder="Last Name" >&ndash;%&gt;--%>
<%--&lt;%&ndash;                                            </div>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                        </div>&ndash;%&gt;--%>
<%--                                    </div>--%>
<%--                                </div>--%>
<%--                                <div class="card-footer text-right">--%>
<%--                                    <button type="submit" class="btn btn-primary">Update Profile</button>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--&lt;%&ndash;                            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">&ndash;%&gt;--%>

<%--&lt;%&ndash;                            </div>&ndash;%&gt;--%>

<%--                            </div>--%>
                        </div>

                    </div>
<%--            </c:forEach>--%>
<%--            </c:when>--%>
<%--            </c:choose>--%>
                </div>
            </div>

        </div>

        <div class="section-body">
            <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            Copyright © 2019 Nail Memmedov
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>

<script src="../assets/bundles/lib.vendor.bundle.js"></script>

<script src="../assets/bundles/fullcalendar.bundle.js"></script>
<script src="../assets/bundles/knobjs.bundle.js"></script>

<script src="../assets/js/core.js"></script>
<script src="assets/js/page/calendar.js"></script>
<script src="assets/js/chart/knobjs.js"></script>
</body>

</html>