<%--
  Created by IntelliJ IDEA.
  User: nailbayramoglu
  Date: 04.01.20
  Time: 18:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>View Employee Task</title>
</head>
<body>
<c:choose>
<c:when test="${not empty taskList}">
    <table border="1" style="color: black">
        <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Description</th>
            <th>Start Date</th>
            <th>End Date</th>
        </tr>
        <c:forEach items="${taskList}" var="tl">
            <tr>
                <td>${tl.id}</td>
                <td>${tl.title}</td>
                <td>${tl.description}</td>
                <td>${tl.startDate}</td>
                <td>${tl.endDate}</td>
            </tr>


            <%-- ID:<br/>
             Title:<br/>
             Description:${tl.description}<br/>
             Start Date:${tl.startDate}<br/>
             End Date:${tl.endDate}<br/>--%>
        </c:forEach>
    </table>
</c:when>
<c:otherwise>
<h1 style="color: deeppink">You Dont Have Any Task</h1>
<table border="1" style="color: red">
    <tr>
        <th>Id</th>
        <th>Title</th>
        <th>Description</th>
        <th>Start Date</th>
        <th>End Date</th>
    </tr>

    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    </c:otherwise>
    </c:choose>
</body>
</html>
