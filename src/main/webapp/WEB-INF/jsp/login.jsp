<%--
  Created by IntelliJ IDEA.
  User: nailbayramoglu
  Date: 24.11.19
  Time: 01:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en" dir="ltr">

<!-- Mirrored from puffintheme.com/craft/soccer/project/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Nov 2019 08:08:56 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="icon" href="favicon.ico" type="image/x-icon"/>

    <title>Login</title>

    <!-- Bootstrap Core and vandor -->
    <link rel="stylesheet" href="../assets/plugins/bootstrap/css/bootstrap.min.css" />

    <!-- Core css -->
    <link rel="stylesheet" href="../assets/css/main.css"/>
    <link rel="stylesheet" href="../assets/css/theme1.css"/>

</head>
<body class="font-montserrat">

<div class="auth">
    <div class="auth_left">
        <div class="card">
            <div class="text-center mb-2">
                <a class="header-brand" href="index-2.html"><i class="fa fa-handshake-o
"></i></a>
            </div>
            <form method="post" action="/login">
                <div class="card-body">
                    <div class="card-title">Login to your account</div>
                    <div class="form-group">
                        <input type="email" class="form-control" aria-describedby="emailHelp" name="email" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Password<a href="forgot-password.html" class="float-right small">I forgot password</a></label>
                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
                        <span>${param.errorMessage}</span>
                    </div>
                    <div class="form-group">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" />
                            <span class="custom-control-label">Remember me</span>
                        </label>
                    </div>
                    <div class="form-footer">
                        <%--                    <a href="" class="btn btn-primary btn-block" title="">Sign in</a>--%>
                        <input type="submit"class="btn btn-primary btn-block" VALUE="SIGN IN">
                    </div>
                </div>
            </form>
            <div class="text-center text-muted">
                Don't have account yet? <a href="/register">Sign up</a>
            </div>
        </div>
    </div>
    <div class="auth_right full_img"></div>
</div>

<script src="../assets/bundles/lib.vendor.bundle.js"></script>
<script src="../assets/js/core.js"></script>

</body>

</html>